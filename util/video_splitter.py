#!/usr/bin/env python3
"""
Simple tool to split a video into multiple shorter ones.

Example usage:
./video_splitter.py ~/this/vid/compilation/test_beta/split_test.mp4 ~/this/vid/compilation/test_beta/splits/ 2000 3
"""
import datetime
import os
import subprocess
import sys

# argument 1: path to video to split
from pathlib import Path

from natsort import natsorted

# temp:::
# video_path = "/home/jort/this/photos/denmark/compilatie/videos_selectie1/"
# video_paths = [os.path.join(video_path, filename) for filename in
#                        os.listdir(video_path)]
# video_paths = natsorted(video_paths, key=lambda y: y.lower())

video_path = "/home/jort/this/photos/kiev"
# video_path = "/home/jort/this/photos/denmark/compilatie/videos_selectie_renamed"
video_path = sys.argv[1] if len(sys.argv) >= 2 else video_path  # use path passed as argument if one is given
video_path = os.path.abspath(video_path)

if os.path.isdir(video_path):
    video_paths = [os.path.join(video_path, filename) for filename in
                   os.listdir(video_path)]
    video_paths = natsorted(video_paths, key=lambda y: y.lower())
else:
    video_paths = [video_path]

for video_path in video_paths:
    if not os.path.isfile(video_path):
        quit(f"File does not exist: {video_path}")

    # argument 2: path for the splitted video results
    split_result_dir = "/home/jort/this/photos/kiev/splits"

    split_result_dir = sys.argv[2] if len(
        sys.argv) >= 3 else split_result_dir  # use path passed as argument if one is given
    split_result_dir = os.path.abspath(split_result_dir)
    if not os.path.isdir(split_result_dir):
        print(f"Created result dir: {split_result_dir}")
        os.mkdir(split_result_dir)

    # argument 3: length for the splits
    split_length = 2000
    split_length = int(sys.argv[3]) if len(sys.argv) >= 4 else split_length

    # argument 4: amount of splits, or percentage of video which should be split
    split_amount = 3
    split_percentage = -1
    split_amount = sys.argv[4] if len(sys.argv) >= 5 else split_amount
    if "%" in f"{split_amount}":
        split_amount = split_amount.replace("%", "")
        split_percentage = int(split_amount)
    else:
        split_amount = int(split_amount)


    def calculate_time_between_splits():
        time_taken_by_splits = split_amount * split_length
        print(f"{time_taken_by_splits}ms taken of {video_length}ms by the splits.")
        time_to_divide = video_length - time_taken_by_splits
        amount_of_padding_needed = split_amount + 1
        print(f"{time_to_divide}ms can be used for {amount_of_padding_needed}x padding.")
        time_for_each_padding = round(time_to_divide / amount_of_padding_needed)
        print(f"That makes each padding {time_for_each_padding}ms long.")
        return time_for_each_padding
        # return round((video_length - split_amount * split_length) / (split_amount + 1))


    def run_cmd(cmd):
        print(f"System call: {cmd}")
        cmd_output = subprocess.run(cmd, shell=True, check=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        return cmd_output.stdout.decode()


    cmd = f"ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 {video_path}"
    video_length = run_cmd(cmd)
    video_length = video_length.replace(".", "")
    video_length = int(video_length) // 1000

    if split_percentage > 0:
        print(f"Calculating the split between videos for {split_percentage}% coverage.")
        split_amount = 1
        while True:
            coverage = (split_length * split_amount * 100) / video_length
            if coverage >= split_percentage:
                print(f"Splitting into {split_amount} parts covers {round(coverage, 2)}% of the video.")
                break
            split_amount += 1
            if split_amount > 100:
                print(f"Splitting into {split_amount} parts covers {round(coverage, 2)}% of the video.")
                print(f"Could not calculate.")
                continue

    # lower split amount if not enough video (failsafe)
    success = True
    while True:
        if split_amount * split_length > video_length:
            split_amount -= 1
            print(f"Not enough video to split, split amount lowered to {split_amount}.")
            continue
        if split_amount == 0:
            print(f"Not enough video for {split_amount}x {split_length}ms.")
            success = False
        break

    if not success:
        continue

    time_between_splits = calculate_time_between_splits()

    print(f"Splitting video of {video_length}ms long in {split_amount} parts "
          f"of {split_length}ms with {time_between_splits}ms in between.")

    for x in range(split_amount):
        video_filename = Path(video_path).stem
        video_extension = Path(video_path).suffix
        part_result_path = os.path.join(split_result_dir, f"{video_filename}_part{x}{video_extension}")
        start_ms = (x + 1) * time_between_splits + split_length * x  # yeah boy gl figuring out this advanced algebra
        end_ms = start_ms + split_length
        print(f"Splitting part {x} from {start_ms}ms to {end_ms}ms to {part_result_path}")

        start_time_string = datetime.datetime.fromtimestamp(start_ms / 1000.0)
        start_time_string = start_time_string - datetime.timedelta(hours=1)
        start_time_string = start_time_string.strftime("%H:%M:%S.%f")
        end_time_string = datetime.datetime.fromtimestamp(end_ms / 1000.0)
        end_time_string = end_time_string - datetime.timedelta(hours=1)
        end_time_string = end_time_string.strftime("%H:%M:%S.%f")
        print(f"Trimming range: {start_time_string} to {end_time_string}.")
        # -c copy somehow results in bad footage
        output = run_cmd(f"ffmpeg -y -ss {start_time_string} -to {end_time_string} "
                         f" -i {video_path} {part_result_path}")
        print(output)
        print(f"Done: {part_result_path} from {start_time_string} to {end_time_string}")

    print(f"Done plitting video of {video_length}ms long in {split_amount} parts "
          f"of {split_length}ms with {time_between_splits}ms in between!")
