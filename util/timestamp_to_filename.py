#!/usr/bin/env python3
"""
Simple tool to set the creation date of files as filename.
I used this to keep the files in order when using the other scripts, because some remove the creation date metadata.
"""
import datetime
import os
import shutil
import subprocess
import sys

# argument 1: path to video to split
import time
from pathlib import Path

from natsort import natsorted

# argument 1: folder with files to rename
source_dir = "/home/jort/this/photos/denmark/compilatie/videos_selectie_renamed"
source_dir = sys.argv[1] if len(sys.argv) >= 2 else source_dir  # use path passed as argument if one is given
source_dir = os.path.abspath(source_dir)
if not os.path.isdir(source_dir):
    quit(f"Folder does not exist: {source_dir}")


file_paths = [os.path.join(source_dir, filename) for filename in os.listdir(source_dir)]
file_paths = natsorted(file_paths, key=lambda y: y.lower())  # sort items same as the file manager does

for file_path in file_paths:
    modified_time = os.path.getmtime(file_path)
    time_string = datetime.datetime.fromtimestamp(modified_time)
    # start_time_string = start_time_string - datetime.timedelta(hours=1)
    time_string = time_string.strftime("%Y%m%d_%H%M%S")
    filename = Path(file_path).stem
    extension = Path(file_path).suffix
    result_filename = f"{time_string}_{filename}{extension}"
    result_file_path = os.path.join(source_dir, result_filename)
    print(f"Renaming {file_path} to {result_file_path}")
    shutil.move(file_path, result_file_path)
