#!/usr/bin/env python3
"""
Extracts the videos from all motion photos within a folder.
Tested on Samsung motion photos from Galaxy S7 and Galaxy S10e.
"""

import os
import shutil
import sys
from pathlib import Path

from natsort import natsorted

target_fps = 60

# argument 1: folder with motion photos
if len(sys.argv) <= 1:
    quit("No source folder provided.")

source_path = os.path.abspath(sys.argv[1])
if not os.path.isdir(source_path):
    quit("Source folder does not exist.")

# argument 2: result folder with extracted mp4s
result_path = os.path.join(source_path, "interpolated_videos")
result_path = sys.argv[2] if len(sys.argv) >= 3 else result_path  # use path passed as argument if one is given
result_path = os.path.abspath(result_path)
if not os.path.isdir(result_path):
    print(f"Creating result folder {result_path}")
    os.mkdir(result_path)

print(f"Started extracting video from motion photos from {source_path} to {result_path}")
files = [os.path.join(source_path, file) for file in os.listdir(source_path)]
files = natsorted(files, key=lambda y: y.lower())  # sort items same as the file manager does
os.chdir("/home/jort/this/repos/arXiv2021-RIFE/")
for motion_photo_filename in files:
    if not motion_photo_filename.endswith(".mp4"):
        print(f"Skipping non-mp4 entry: {motion_photo_filename}")
        continue
    video_path = os.path.join(source_path, motion_photo_filename)
    os.system(f"python3 /home/jort/this/repos/arXiv2021-RIFE/inference_video.py --exp=1 --fps={target_fps} --video={video_path}")

print(f"Moving extracted videos to result folder.")
files = [os.path.join(source_path, file) for file in os.listdir(source_path)]
files = natsorted(files, key=lambda y: y.lower())  # sort items same as the file manager does
files = [file for file in files if file.endswith("fps.mp4")]
for filename in files:
    from_path = os.path.join(source_path, filename)
    stem = Path(from_path).stem
    to_path = os.path.join(result_path, f"{stem}.mp4")
    print(f"Moving {from_path} to {to_path}")
    shutil.move(from_path, to_path)

print(f"Done interpolating videos!")
print(f"Results saved to {result_path}")
