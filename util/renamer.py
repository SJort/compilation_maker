#!/usr/bin/env python3
"""
Simple tool to rename all files within a folder.
I used this to make the order more clear,
and ffmpeg is not happy with all filenames, for example ones with '(copy)' in them.
I edit the code to let it do what I want.
"""

import os
import sys
import time
import pathlib
import shutil

from natsort import natsorted

rename_dir = "/home/jort/this/photos/denmark/compilatie/merged_selectie"
rename_dir = "/home/jort/this/photos/denmark/compilatie/temp"
rename_dir = sys.argv[1] if len(sys.argv) == 2 else rename_dir  # use path passed as argument if one is given
rename_dir = os.path.abspath(rename_dir)
print(f"Renaming everything in {rename_dir}")
if not os.path.isdir(rename_dir):
    quit("Path does not exists.")

file_paths = [os.path.join(rename_dir, filename) for filename in os.listdir(rename_dir)]
file_paths = natsorted(file_paths, key=lambda y: y.lower())  # sort items same as the file manager does

count = 10
last_name = file_paths[0].split("part")[0]
count_count = 0
for file_path in file_paths:
    filename = pathlib.Path(file_path).stem
    extension = pathlib.Path(file_path).suffix

    name = file_path.split("part")[0]
    if name != last_name:
        count += 20
    last_name = name

    # you can add unique nanoseconds to filename, because when the result path already exists, it is overwritten
    # this is a problem, because os.listdir gives the files in a random order. to solve this, I natsort them
    new_file_path = os.path.join(rename_dir, f"video{count}_{filename}{extension}")
    print(f"Renaming: {file_path} to {new_file_path}")
    if os.path.isfile(new_file_path):
        quit(f"Path already exists: {new_file_path}")
    shutil.move(file_path, new_file_path)
print(f"Done.")
