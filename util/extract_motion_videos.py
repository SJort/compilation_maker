#!/usr/bin/env python3
"""
Extracts the videos from all motion photos within a folder.
Tested on Samsung motion photos from Galaxy S7 and Galaxy S10e.
"""

import os
import shutil
import sys
from pathlib import Path

from natsort import natsorted

# argument 1: folder with motion photos
if len(sys.argv) <= 1:
    quit("No source folder provided.")

source_path = os.path.abspath(sys.argv[1])
if not os.path.isdir(source_path):
    quit("Source folder does not exist.")

# argument 2: result folder with extracted mp4s
result_path = os.path.join(source_path, "extracted_motion_videos")
result_path = sys.argv[2] if len(sys.argv) >= 3 else result_path  # use path passed as argument if one is given
result_path = os.path.abspath(result_path)
if not os.path.isdir(result_path):
    print(f"Creating result folder {result_path}")
    os.mkdir(result_path)

print(f"Started extracting video from motion photos from {source_path} to {result_path}")
files = [os.path.join(source_path, file) for file in os.listdir(source_path)]
files = natsorted(files, key=lambda y: y.lower())  # sort items same as the file manager does
for motion_photo_filename in files:
    if not motion_photo_filename.endswith(".jpg"):
        print(f"Skipping non-jpg entry: {motion_photo_filename}")
        continue
    motion_photo_path = os.path.join(source_path, motion_photo_filename)
    os.system(f"motion_photo_splitter  {motion_photo_path}")

print(f"Deleting extracted photos.")
files = [os.path.join(source_path, file) for file in os.listdir(source_path)]
files = natsorted(files, key=lambda y: y.lower())  # sort items same as the file manager does
files = [file for file in files if file.endswith(".jpg.jpg")]
for filename in files:
    to_delete = os.path.join(source_path, filename)
    print(f"Deleting {to_delete}")
    os.remove(to_delete)

print(f"Moving extracted videos to result folder.")
files = [os.path.join(source_path, file) for file in os.listdir(source_path)]
files = natsorted(files, key=lambda y: y.lower())  # sort items same as the file manager does
files = [file for file in files if file.endswith(".jpg.mp4")]
for filename in files:
    from_path = os.path.join(source_path, filename)
    stem = Path(from_path).stem
    stem = Path(stem).stem  # two times because it has to file extensions: .jpg.mp4
    to_path = os.path.join(result_path, f"{stem}.mp4")
    print(f"Moving {from_path} to {to_path}")
    shutil.move(from_path, to_path)

print(f"Done extraction videos from motion photos!")
print(f"Results saved to {result_path}")
