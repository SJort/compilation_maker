#!/usr/bin/env python3
"""
Extracts the videos from all motion photos within a folder.
Tested on Samsung motion photos from Galaxy S7 and Galaxy S10e.
"""

import os
import shutil
import sys
from pathlib import Path

from natsort import natsorted

source_dir = "/home/jort/this/vid/compilation/test_beta/processed_videos"
source_dir = os.path.abspath(sys.argv[1]) if len(sys.argv) >= 2 else source_dir
if not os.path.isdir(source_dir):
    quit("Source folder does not exist.")


paths = [os.path.join(source_dir, filename) for filename in
         os.listdir(source_dir)]
paths = natsorted(paths, key=lambda y: y.lower())

for path in paths:
    # cmd = f"ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 {path}"
    # vid_length = os.popen(cmd).read()
    # vid_length = vid_length.replace(".", "")
    # vid_length = int(vid_length) // 1000

    cmd = f"ffprobe -v 0 -of csv=p=0 -select_streams v:0 -show_entries stream=r_frame_rate {path}"
    print(cmd)
    vid_fps = os.popen(cmd).read()
    vid_fps = int(vid_fps.split("/")[0]) / int(vid_fps.split("/")[1])
    vid_fps = round(vid_fps, 2)

    # print(f"{path}: length={vid_length}ms, fps={vid_fps}")
    if vid_fps < 30:
        print(f"{path}: fps={vid_fps}")
        break


