#!/usr/bin/env python3
"""
Creates the timed video compilation.
See comments for step explanation.
Example usage:
 ./compilation_maker.py ~/this/vid/compilation/source/ ~/this/vid/compilation/gimme_love.mp3 ~/this/vid/compilation/gimme_love_timestamps.yaml ~/this/vid/compilation/

"""
import datetime
import logging
import os
import re
import shutil
import subprocess
import sys
import time
import traceback

import yaml
from natsort import natsorted

# adjustable variables which cannot be set by command line arguments (yet)
target_fps = 60  # fps of end result, frames get duplicated if input is less (which means it does make it smoother)
# resolution of result. inputs get scaled and cropped to this width. recommended to set the same as your input vids
resolution_width = 1440
resolution_height = 1080
slow_down_too_short_videos = False  # false: loops short videos instead
trim_start_ms = 1  # trim too long videos starting from this timestamp, if there is enough room
flickering_padding_ms = 600  # how late into the video the flickering starts (and ends)

runtime_id = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
log_file_path = f"log/compilation_maker_log_{runtime_id}"
# write logs to file, and to stream (just log.info them), log everything with severity of debug and higher like info
# logging.basicConfig(handlers=[logging.FileHandler(log_file_path), logging.StreamHandler()], level=logging.DEBUG)
logging.basicConfig(handlers=[logging.FileHandler(log_file_path)], level=logging.DEBUG)
log = logging.getLogger("compilation_maker")


def print_log(msg, end="\n"):
    print(f"{time.strftime('%H:%M:%S')}   {msg}", end=end)
    log.info(msg)


def error(msg):
    log.critical(msg)
    print(msg)
    quit(1)


def run_cmd(cmd):
    log.info(f"System call: {cmd}")
    try:
        cmd_output = subprocess.run(cmd, shell=True, check=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        cmd_output = cmd_output.stdout.decode()
        log.info(f"Process output: {cmd_output}")
    except subprocess.CalledProcessError as e:
        cmd_output = e.output.decode()
        log.error(f"Error process output: {cmd_output}")
        error(f"Error command:{cmd}\n{traceback.format_exc()}\n{cmd_output}\n"
              f"Process failed with exit code {e.returncode}.")
    return cmd_output


def ffmpeg_cmd(arguments_string):
    # wrapper function to make calls to ffmpeg
    cmd = f"ffmpeg -y {arguments_string}"  # -y: overwrite without asking
    return run_cmd(cmd)


def create_folder_if_not_exists(path, msg):
    if os.path.isdir(path):
        return
    os.mkdir(path)
    log.info(f"Created directory for {msg}: {path}")


try:
    # argument1: path to folder with videos
    if len(sys.argv) <= 1:
        error("No video source folder provided.")
    vid_source_dir = os.path.abspath(sys.argv[1])
    if not os.path.isdir(vid_source_dir):
        error("Source folder does not exist.")
    input_vid_paths = [os.path.join(vid_source_dir, filename) for filename in os.listdir(vid_source_dir) if
                       filename.endswith(".mp4")]
    input_vid_paths = natsorted(input_vid_paths, key=lambda y: y.lower())

    # argument2: path to music
    if len(sys.argv) <= 2:
        error("No music path provided.")
    music_path = os.path.abspath(sys.argv[2])
    if not os.path.isfile(music_path):
        error(f"No valid music path: {music_path}")

    # argument3: path to yaml file with music timestamps
    if len(sys.argv) <= 3:
        error("No music timestamps provided.")
    timestamp_path = os.path.abspath(sys.argv[3])
    if not os.path.isfile(timestamp_path):
        error(f"No valid timestamp: {timestamp_path}")
    timestamp_dict = yaml.safe_load(open(timestamp_path))  # read timestamps from yaml file
    type1_timestamps = timestamp_dict["type1"]
    type2_timestamps = timestamp_dict["type2"]

    # stop if not enough videos
    amount_of_type1_timestamps = len(type1_timestamps)
    amount_of_type2_timestamps = len(type2_timestamps)
    amount_of_videos = len(input_vid_paths)
    log.info(f"Amount of timestamps: {amount_of_type1_timestamps}, amount of videos: {amount_of_videos}.")
    if amount_of_type1_timestamps > amount_of_videos:
        log.warning(f"Not enough videos to cover all the timestamps.")

    # argument4: path for temporary and result files
    if len(sys.argv) <= 4:
        error("No working folder provided.")
    working_path = os.path.abspath(sys.argv[4])
    if not os.path.isdir(working_path):
        os.mkdir(working_path)
        log.info(f"Created non-existing working directory: {working_path}")

    # argument5 (optional): steps to skip
    skip_steps_until = 0
    skip_steps_until = int(sys.argv[5]) if len(
        sys.argv) >= 6 else skip_steps_until  # use path passed as argument if one is given
    if skip_steps_until > 0:
        log.info(f"WARNING: skipping steps until step {skip_steps_until}.")

    # CREATED FOLDERS:
    # folder for trimmed videos (videos which now have the correct length)
    trimmed_vids_dir = os.path.join(working_path, "trimmed_videos")
    create_folder_if_not_exists(trimmed_vids_dir, "trimmed videos")
    # folder for input videos which are fully processed: have correct fps, overlay, proper trim, etc
    processed_vids_dir = os.path.join(working_path, "processed_videos")
    create_folder_if_not_exists(processed_vids_dir, "processed input videos")
    # folder for all the combined extracted frames from the processed videos
    extracted_frames_dir = os.path.join(working_path, "extracted_frames")
    create_folder_if_not_exists(extracted_frames_dir, "extracted frames")
    # folder for the exact frames of the end product
    result_frames_dir = os.path.join(working_path, "result_frames")
    create_folder_if_not_exists(result_frames_dir, "end product frames")

    # CREATED FILES:
    # file with paths of frames for ffmpeg to construct the result video with
    result_vid_paths_txt_path = os.path.join(working_path, "result_vid_paths.txt")
    # the result video file path
    result_vid_path = os.path.join(working_path, f"result_{runtime_id}.mp4")

    # USED FOLDERS AND FILES
    src_dir = os.path.join(os.getcwd(), "src")
    font_path = os.path.join(src_dir, "font.ttf")
    if not os.path.isfile(font_path):  # I could also add just not to add text
        error(f"Cannot find file {font_path}")

    print_log(f"Started generating compilation.\n"
              f"{amount_of_videos} videos found, and {amount_of_type1_timestamps} timestamps.\n"
              f"This process takes about 10 minutes on my above average computer for 300 videos.")

    """
    Step 1:
    - Trim videos to desired length
    """
    print_log(f"Step 1: edit videos to required length.")
    previous_timestamp = 0
    if skip_steps_until >= 1:
        print_log(f"Skipping step 1!")
    else:
        log.info("Clearing trimmed videos folder.")
        shutil.rmtree(trimmed_vids_dir)
        os.mkdir(trimmed_vids_dir)
        for video_number in range(min(amount_of_videos, amount_of_type1_timestamps)):
            timestamp = type1_timestamps[video_number]
            print_log(f"Editing length of input video {video_number} / {amount_of_videos}", end="\r")
            input_vid_path = input_vid_paths[video_number]
            input_vid_filename = os.path.basename(input_vid_path)
            temp_vid_slowed_down_path = os.path.join(trimmed_vids_dir, f"video{video_number}_slowed_down.mp4")
            temp_vid_trimmed_path = os.path.join(trimmed_vids_dir, f"video{video_number}_trimmed.mp4")

            # calculate length this video should be
            required_length = timestamp - previous_timestamp
            # 1.2 trim slack, makes the videos a bit longer than needed, as we should not lack frames in end video
            required_length = round(required_length * 1.2)
            previous_timestamp = timestamp

            # get approximate video length in ms
            cmd = f"ffprobe -v error -show_entries " \
                  f"format=duration -of default=noprint_wrappers=1:nokey=1 {input_vid_path}"
            input_vid_length = run_cmd(cmd)
            input_vid_length = input_vid_length.replace(".", "")
            input_vid_length = int(input_vid_length) // 1000
            duration_delta = input_vid_length - required_length
            log.info(f"{input_vid_filename}: duration={input_vid_length}ms, needs to be {required_length}ms.")

            # if duration is shorter than required duration, slow down the video to required duration
            if duration_delta < 0 and slow_down_too_short_videos:
                slow_factor = round(required_length / input_vid_length, 2)
                log.info(f"Slowing down {slow_factor}x.")
                cmd = f"-i {input_vid_path} -filter:v 'setpts={slow_factor}*PTS' {temp_vid_slowed_down_path}"
                ffmpeg_cmd(cmd)
            # else trim the video to required length
            else:
                start_ms = trim_start_ms
                if input_vid_length - trim_start_ms < required_length:
                    # don't trim from later than the beginning of vid if this makes the trim result too short
                    log.info(f"Trimming the video starting from {trim_start_ms} "
                             f"makes it too short, starting from 0 instead.")
                    start_ms = 0
                end_ms = start_ms + required_length
                cmd = f"-ss {start_ms}ms -to {end_ms}ms -i {input_vid_path} -c copy {temp_vid_trimmed_path}"
                ffmpeg_cmd(cmd)

    """
    Step 2:
    - Set video resolution.
    """
    print_log(f"Step 2: set fps, resolution and text of videos.")
    if skip_steps_until >= 2:
        print_log(f"Skipping step 2!")
    else:

        # clear the processed frames folder, otherwise result bad if timestamp file is changed
        log.info("Clearing processed videos folder.")
        shutil.rmtree(processed_vids_dir)
        os.mkdir(processed_vids_dir)
        input_vid_paths = [os.path.join(trimmed_vids_dir, filename) for filename in os.listdir(trimmed_vids_dir) if
                           filename.endswith(".mp4")]
        input_vid_paths = natsorted(input_vid_paths, key=lambda y: y.lower())
        amount_of_videos = len(input_vid_paths)

        for video_number in range(amount_of_videos):
            print_log(f"Editing video {video_number} / {amount_of_videos}", end="\r")
            input_vid_path = input_vid_paths[video_number]
            input_vid_filename = os.path.basename(input_vid_path)
            video_overlay_text = f"{video_number}"
            temp_vid_processed_path = os.path.join(processed_vids_dir, f"video{video_number}_processed.mp4")

            # set the fps, scale video to desired dimensions keeping aspect ratio, crop off the excess, draw the text
            cmd = f"-i {input_vid_path} -vf \"fps={target_fps},scale={resolution_width}:{resolution_height}:" \
                  f"force_original_aspect_ratio=increase," \
                  f"crop={resolution_width}:{resolution_height},drawtext=fontfile={font_path}:" \
                  f"text='{video_overlay_text}':fontcolor=white:fontsize=24:" \
                  f"box=1:boxcolor=black@0.5:boxborderw=3:" \
                  f"x=3:y=3\" {temp_vid_processed_path}"
            ffmpeg_cmd(cmd)

    """
    Step 3:
    - extract all the frames from the edited videos, to reconstruct our end result video from
    """
    print_log(f"Step 3: extract frames from the edited input videos.")
    if skip_steps_until >= 3:
        print_log(f"Skipping step 3!")
    else:
        # clear the extracted frames folder, otherwise result bad if timestamp file is changed
        log.info("Clearing extracted frames folder.")
        shutil.rmtree(extracted_frames_dir)
        os.mkdir(extracted_frames_dir)

        input_vid_paths = [os.path.join(processed_vids_dir, filename) for filename in os.listdir(processed_vids_dir) if
                           filename.endswith(".mp4")]
        input_vid_paths = natsorted(input_vid_paths, key=lambda y: y.lower())
        amount_of_videos = len(input_vid_paths)
        log.info(f"Extracting frames from processed videos.")
        video_number = 0
        for video_number in range(amount_of_videos):
            processed_vid_path = input_vid_paths[video_number]
            print_log(f"Extracting frames from video {video_number} / {amount_of_videos}", end="\r")
            cmd = f"-i {processed_vid_path} -qscale:v 2 {extracted_frames_dir}/video{video_number}_frame%03d.jpg"
            ffmpeg_cmd(cmd)

    """
    Step 4:
    - determine which frames go in the result video, so the timing is exactly as we want it
    """
    print_log(f"Step 4: determining frames of result video.")
    if skip_steps_until >= 4:
        print_log(f"Skipping step 4!")
    else:
        # clear the result frames folder, otherwise result bad if timestamp file is changed
        log.info("Clearing result frames folder.")
        shutil.rmtree(result_frames_dir)
        os.mkdir(result_frames_dir)

        log.info(f"Determining video frames of end product.")
        delay_between_frames = 1000 / target_fps  # 30 fps means 33.33 ms between each frame
        current_ms = 0  # the exact moment where we are with writing frames in our result video
        video_number = 0  # also the index of the timestamp list
        type2_index = 0  # keep track for flickering effect, we need the closest flicker timestamp
        last_flicker_timestamp = 0
        #  the timestamp we are currently writing frames towards,
        #  so when current_ms exceeds this timestamp, go write frames from the next video to the next timestamp
        current_timestamp = type1_timestamps[video_number]
        current_video_frame_index = 0  # at which frame of the input video we are
        amount_of_source_frames = len(os.listdir(extracted_frames_dir))  # we need to loop a maximum am
        result_video_length = type1_timestamps[-1]  # the music marker puts one timestamp at the exact end of the video
        writing_inverted = False  # set to true of no more frames, so we start writing frames backwards for loop effect
        # add frame count to filenames, to keep correct order and against duplicate filenames in case of video loop
        current_video_ms = 0  # ms within the current video being handled
        frame_count = 0  # also the amount of times looped
        # keep looping until all frames are determined of result video
        # add little buffer so music does not get cut off, ffmpeg automatically trims this with --shortest or whatever
        while current_ms < result_video_length + 500:
            print_log(f"Processed second {round(current_ms // 1000)} / {(result_video_length + 500) // 1000}"
                      f" of result video.", end="\r")
            current_ms += delay_between_frames
            current_video_ms += delay_between_frames
            # when the total time of all the currently added frames exceeds this timestamp, go to the next video
            if current_ms > current_timestamp:
                log.info(f"Going to next video and timestamp.")
                video_number += 1
                if video_number >= len(type1_timestamps):
                    log.info(f"All timestamps processed!")
                    break
                current_timestamp = type1_timestamps[video_number]
                current_video_frame_index = 0
                writing_inverted = False
                current_video_ms = 0

            # determine which frames in the folder belong to the current video
            regex_pattern = f".*?video{video_number}_frame\d\d\d.jpg"
            regex = re.compile(regex_pattern)
            vid_frames = [os.path.join(extracted_frames_dir, filename) for filename in os.listdir(extracted_frames_dir)
                          if
                          regex.match(filename)]
            vid_frames = natsorted(vid_frames, key=lambda y: y.lower())
            amount_of_vid_frames = len(vid_frames)

            log.info(f"Current ms={current_ms}, video={video_number}, "
                     f"timestamp={current_timestamp}, frames={amount_of_vid_frames}, "
                     f"current_frame_index={current_video_frame_index}, current_video_ms={current_video_ms}")

            if amount_of_vid_frames < 2:
                # this case can happen when there are less videos than timestamps
                log.warning(f"Not enough frames in video! Going to next video.")
                video_number += 1
                continue

            # move the frame to the result folder
            from_path = vid_frames[current_video_frame_index]
            to_filename = f"{frame_count}_video{video_number}_frame{current_video_frame_index}.jpg"
            to_path = os.path.join(result_frames_dir, to_filename)
            log.info(f"Copying {from_path} to {to_path}")
            shutil.copy2(from_path, to_path)  # copy2 keeps metadata or whatever

            # flickering effect
            """
            Flickering effect
            get time to next video
            IF: do it after 200ms into current video, until 200 ms before next video
            get nearest rounded up flicker timestamp
            if reached, revert writing
            """

            try:
                ms_to_next_video = type1_timestamps[video_number + 1] - current_ms
            except IndexError:
                log.info(f"This out of bounds error again: {video_number}, {len(type1_timestamps)}")
                ms_to_next_video = 0
            nearest_flicker_timestamp = 0

            if current_video_ms > flickering_padding_ms and ms_to_next_video > 1:
                log.info("Handling flickering effect.")

                # determine when the next flicker needs to occur
                for x in range(type2_index, amount_of_type2_timestamps):
                    t = type2_timestamps[x]
                    if t > current_ms:
                        nearest_flicker_timestamp = t
                        log.info(f"Found nearest flickering timestamp: {t}")
                        type2_index = x  # just for performance
                        if last_flicker_timestamp is not t:
                            writing_inverted = not writing_inverted
                            log.info(
                                f"This is not the last timestamp, inverting frames {'backwards' if writing_inverted else 'forwards'}!")
                            last_flicker_timestamp = t
                        break

            # loop the video backwards if no more frames to work with
            if current_video_frame_index >= amount_of_vid_frames - 1 and not writing_inverted:
                log.info(f"No more frames, looping backward!")
                writing_inverted = True
            if current_video_frame_index <= 0:
                log.info(f"No more frames, looping forwards again!")
                writing_inverted = False

            # handle input vids current frame index
            if not writing_inverted:
                current_video_frame_index += 1
            else:
                current_video_frame_index -= 1

            frame_count += 1

    """
    Step 5:
    - Construct the result video from our times frames and put the music under it.
    """

    print_log(f"Step 5: creating result video from determined frames. "
              f"This takes 2 minutes on my above average computer.")
    if skip_steps_until >= 5:
        print_log(f"Skipping step 5!")
    else:
        log.info(f"Creating file with all the paths of the images needed to make the result video.")
        result_frames_paths = [os.path.join(result_frames_dir, filename) for filename in os.listdir(result_frames_dir)]
        result_frames_paths = natsorted(result_frames_paths, key=lambda y: y.lower())
        with open(result_vid_paths_txt_path, "w") as file:
            for result_frame_path in result_frames_paths:
                if not os.path.exists(result_frame_path):
                    # sanity check, result really should not miss any frames
                    error(f"Result frame path does not exist: {result_frame_path}")
                file.write(f"file {result_frame_path}\n")

        log.info(f"Generating video from frames.")
        cmd = f"-r {target_fps} -f concat -safe 0  -i {result_vid_paths_txt_path} -i {music_path} " \
              f"-c:a copy -shortest -c:v libx264 -pix_fmt yuv420p {result_vid_path}"
        ffmpeg_cmd(cmd)

    print_log(f"Video compilation generated! Path: {result_vid_path}")

except KeyboardInterrupt:
    print_log(f"Program manually stopped!")
