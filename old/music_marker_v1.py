#!/usr/bin/env python3
"""
Tool to mark timestamps on a music.
Plays the music when spacebar is pressed.
Each time a key is pressed, the timestamp of that key press is recorded.
When spacebar is pressed again, or when the song ends, the timestamps are written to a .yaml file.
"""
import tkinter as tk
import pygame
import time
import yaml

root = tk.Tk()

running = True


def stop():
    global running
    root.destroy()
    running = False
    print(f"Stopped.")


root.bind("<Escape>", lambda x: stop())

pygame.init()
pygame.mixer.init()

recorded_keys = []
start_time = 0
started = False
SONG_END = pygame.USEREVENT
pygame.mixer.music.load("../src/music.mp3")


def on_key(event):
    global start_time
    pressed_key = str(event.char)
    if started:
        elapsed = round(time.time() * 1000) - start_time
        recorded_keys.append(elapsed)
        bpm_label_text.set(f"BPM: {get_bpm()}")
    else:
        print(f"Key pressed but no song playing.")


def on_end():
    global started
    started = False
    print(f"Recording ended. Recordings: {recorded_keys}")
    stop_song()
    file = open("../src/output.yaml", "w")
    yaml.safe_dump(recorded_keys, file)


def space_toggle(event):
    global started
    global start_time
    if not started:
        started = True
        start_time = round(time.time() * 1000)
        play_song()
    else:
        started = False
        on_end()


root.bind("<Key>", on_key)
root.bind("<space>", space_toggle)


def get_bpm():
    global recorded_keys
    sum = 0
    count = 0

    for x in range(2, len(recorded_keys)):
        sum += recorded_keys[x] - recorded_keys[x - 1]
        count += 1

    if count == 0:
        return 0
    avg_ms = sum / count
    if avg_ms == 0:
        return 0
    bpm = round((60 / avg_ms) * 1000, 1)
    return bpm


def play_song():
    pygame.mixer.music.play()
    pygame.mixer.music.set_endevent(SONG_END)


def stop_song():
    pygame.mixer.music.stop()
    pygame.mixer.music.rewind()
    pygame.event.clear()


title = tk.Label(root, text="Music Marker", font=("times new roman", 50, "bold"))
title.pack(side=tk.TOP, fill=tk.X)

tk.Label(root, text="Press space to start / stop recording song",
         font=("times new roman", 10, "bold")).pack(pady=20)

bpm_label_text = tk.StringVar()
bpm_label = tk.Label(root, textvariable=bpm_label_text)
bpm_label.pack(pady=20)
bpm_label_text.set(f"BPM: ")

while True:
    if not running:
        quit()
    root.update()
    root.update_idletasks()
    if started:
        for pygame_event in pygame.event.get():
            if pygame_event.type == SONG_END:
                print(f"Song ended.")
                on_end()
