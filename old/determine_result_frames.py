import logging
import os
import re
import shutil

import cv2
from PIL import Image
from natsort import natsorted

import file_manipulation as fm
import general_functions as gf
from video import Video
from set_video_data import add_frames_to_video_list
import image_manipulation as im

log = logging.getLogger("result_frames")


def determine_result_frames(source_frames_dir, result_frames_dir, music_timestamps, timestamps_per_video,
                            highlight_timestamp_after_index,
                            video_list, target_fps):
    gf.print_log(f"Determining frames of result video.")
    fm.realise_dir(result_frames_dir, "frames to create the result video from")
    frame_ms = 1000 / target_fps  # 30 fps means 33.33 ms between each frame
    output_ms = 0  # the exact moment where we are with writing frames in our result video
    add_frames_to_video_list(video_list=video_list, frames_dir=source_frames_dir, target_fps=target_fps)

    current_vid_index = 0  # the index of the source video being used
    current_vid = video_list[current_vid_index]
    # the duration of the output video, for process bar, for now the last music timestamp,
    # the music marker puts one timestamp at the exact end of the video
    result_vid_length = music_timestamps[-1]
    frame_count = 0  # how many frames are written to result
    # the index of the first timestamp being used for the current range
    # start value so range rotation in the loop triggers on first run
    timestamp_index = -timestamps_per_video

    output_video_width, output_video_height = Image.open(current_vid.frame_paths[0]).size
    log.info(f"Frame dimensions: {output_video_width}x{output_video_width}")
    slowmo_factor = 2.0
    speedup_factor = 2.0
    fast_mode = True
    timestamp_range_end_ms = -1
    font = cv2.FONT_HERSHEY_SIMPLEX
    beat_count = 0

    while 1:
        # we reserve a timestamp range for each video. when out of range, go to next range and video
        if timestamp_range_end_ms <= output_ms:
            log.info("Reached the end of the current timestamp range, switching to next range and video.")

            # switch to next timestamp range
            timestamp_index = timestamp_index + timestamps_per_video
            if timestamp_index >= len(music_timestamps) + 1:
                log.info("No more timestamps available for the rest of the videos.")
                break
            end_timestamp_index = timestamp_index + timestamps_per_video
            if end_timestamp_index >= len(music_timestamps) + 1:  # this will probably always trigger first
                log.info("No more timestamps available to cover this video to the end of the timestamp range.")
                break
            timestamp_range_end_ms = music_timestamps[timestamp_index + timestamps_per_video]
            log.info(f"Timestamp index is now {timestamp_index}. End at {timestamp_range_end_ms}ms.")

            # switch to next video
            current_vid_index += 1
            if current_vid_index >= len(video_list) - 1:
                log.info("No more videos available for the rest of the timestamps.")
                break
            current_vid = video_list[current_vid_index]

            # at which music timestamp the highlight should happen
            highlight_timestamp_index = timestamp_index + highlight_timestamp_after_index
            highlight_timestamp = music_timestamps[highlight_timestamp_index]
            # how many ms that timestamp is after the first timestamp of the current timestamp range
            highlight_timestamp_delta = highlight_timestamp - music_timestamps[timestamp_index]
            # how many ms from the beginning of this video should be trimmed to
            # have the highlight sync up with the above determined timestamp
            current_video_prefix_trim = current_vid.highlight_timestamp - highlight_timestamp_delta
            log.info(f"{highlight_timestamp_index=}, {highlight_timestamp=}, "
                     f"{highlight_timestamp_delta=}, {current_video_prefix_trim=}")
            current_vid.discard_ms = current_video_prefix_trim
            if current_vid.discard_ms < 0:
                log.warning(f"Input video too short: {current_vid}")
                break

            # determine at which times within this video the highlight effect should be triggered (slow motion/reversal)
            current_vid.highlight_effect_start_ms = music_timestamps[highlight_timestamp_index - 0]
            current_vid.highlight_effect_stop_ms = music_timestamps[highlight_timestamp_index + 1]
            log.info(f"Highlight special effect: "
                     f"from {current_vid.highlight_effect_start_ms}ms to {current_vid.highlight_effect_stop_ms}ms.")

            # at which times a video effect occurs (speedup)
            current_vid.effect_start_ms = music_timestamps[highlight_timestamp_index - 5]
            current_vid.effect_stop_ms = music_timestamps[highlight_timestamp_index - 4]
            log.info(f"Video effect from {current_vid.effect_start_ms}ms to {current_vid.effect_stop_ms}ms.")

        # discard the first few frames of this video to have highlight sync with beat
        if current_vid.read_ms < current_vid.discard_ms:
            current_vid.read_ms += frame_ms
            current_vid.frame_index += 1
            continue
        else:
            gf.print_log(f"Processing second {round(output_ms // 1000)} / {result_vid_length // 1000}"
                         f" of result video", end="\r")

        # write the frame
        if len(current_vid.frame_paths) - 1 <= current_vid.frame_index:
            log.info("No more frames to write.")  # idk why this happens
            break
        from_path = current_vid.frame_paths[int(current_vid.frame_index)]
        to_filename = f"{frame_count}_video{current_vid.index}_frame{int(current_vid.frame_index)}.jpg"
        to_path = os.path.join(result_frames_dir, to_filename)
        shutil.copy2(from_path, to_path)  # copy2 keeps metadata or something
        log.info(f"Copied {from_path} to {to_path}")

        # apply effects to video #######################################################################################
        slowmo = False
        reverse = False
        speedup = False

        # highlight effect trigger
        if current_vid.highlight_effect_start_ms <= output_ms <= current_vid.highlight_effect_stop_ms:
            log.info("Video effect triggered for highlight.")
            # slowmo = True
            reverse = True

        # regular effect trigger
        if current_vid.effect_start_ms <= output_ms <= current_vid.effect_stop_ms:
            log.info("Video effect triggered.")
            speedup = True

        # highlight trigger edits
        if slowmo or reverse:
            log.info("Applying highlight video effects.")
            img = cv2.imread(to_path)
            # img = im.increase_brightness(img, 30)
            cv2.putText(img=img, text=f"Kill {current_vid.index}", org=(360, 110), fontFace=font,
                        fontScale=2, color=(0, 255, 255), thickness=3, lineType=cv2.LINE_AA)
            cv2.imwrite(to_path, img)
        # regular trigger edits
        elif speedup:
            log.info("Applying speedup effects.")
            img = cv2.imread(to_path)
            cv2.putText(img=img, text=">>", org=(360, 110), fontFace=font,
                        fontScale=3, color=(0, 255, 255), thickness=3, lineType=cv2.LINE_AA)
            # img = im.increase_brightness(img, 0)
            cv2.imwrite(to_path, img)

        # video effects for each frame #################################################################################

        # increase brightness at beat / music timestamps
        for timestamp in music_timestamps:
            # how many frames before the frame the effect actually should be in the effect should be triggered
            frames_before = 3
            if output_ms - frame_ms * (frames_before + 0.1) <= timestamp <= output_ms - frame_ms * frames_before + 1:
                log.info("Editing frame on beat.")
                img = cv2.imread(to_path)
                img = im.increase_brightness(img, 4)
                beat_count += 1
                cv2.imwrite(to_path, img)

        # put beat indicator
        img = cv2.imread(to_path)
        cv2.putText(img=img, text=f"{beat_count % 4 + 1}", org=(output_video_width - 50, 50),
                    fontFace=font,
                    fontScale=2, color=(0, 255, 255), thickness=2, lineType=cv2.LINE_AA)

        # put input video filename
        cv2.putText(img=img, text=f"{current_vid.filename}", org=(10, output_video_height - 10), fontFace=font,
                    fontScale=1, color=(0, 0, 0), thickness=1, lineType=cv2.LINE_AA)
        cv2.imwrite(to_path, img)

        # update the current videos read progress ######################################################################
        if slowmo:
            current_vid.read_ms += frame_ms / slowmo_factor
            current_vid.frame_index += 1.0 / slowmo_factor
        elif reverse:
            current_vid.read_ms += frame_ms
            current_vid.frame_index -= 1.0
        elif speedup:
            current_vid.read_ms += frame_ms * speedup_factor
            current_vid.frame_index += 2.0
        else:
            current_vid.read_ms += frame_ms
            current_vid.frame_index += 1.0

        output_ms += frame_ms
        frame_count += 1

    amount = len([name for name in os.listdir(result_frames_dir)
                  if os.path.isfile(os.path.join(result_frames_dir, name))])
    gf.print_log(f"Written {frame_count} frames for result video! The folder now has {amount} files.")
