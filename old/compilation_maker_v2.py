#!/usr/bin/env python3
"""
Creates the timed video compilation.
See comments for step explanation.
Example usage:
./compilation_maker.py ~/this/vid/compilation/test_beta/source_videos/ ~/this/vid/compilation/test_beta/gimme_love.mp3 ~/this/vid/compilation/test_beta/gimme_love_timestamps.yaml ~/this/vid/compilation/test_beta/

Set resolution without changing aspect ratio (cut off excess)
ffmpeg -i aspect_test.mp4 -vf "scale=1280:720:force_original_aspect_ratio=increase,crop=1280:720" res.mp4
"""
import datetime
import logging
import os
import re
import shutil
import subprocess
import sys
import time
import traceback

import yaml
from natsort import natsorted

try:
    runtime_id = time.time_ns()  # added to files which should be unique for each run because they should not be reused
    log_file_path = f"log/compilation_maker_log_{runtime_id}"
    # write logs to file, and to stream (just log.info them), log everything with severity of debug and higher like info
    # logging.basicConfig(handlers=[logging.FileHandler(log_file_path), logging.StreamHandler()], level=logging.DEBUG)
    logging.basicConfig(handlers=[logging.FileHandler(log_file_path)], level=logging.DEBUG)
    log = logging.getLogger("compilation_maker")


    def error(msg):
        log.critical(msg)
        print(f"\nCritical error running compilation maker:")
        print(msg)
        print(f"Program stopped because of this error.")
        quit(1)


    # argument1: path to folder with videos
    if len(sys.argv) <= 1:
        error("No video source folder provided.")
    vid_source_dir = os.path.abspath(sys.argv[1])
    if not os.path.isdir(vid_source_dir):
        error("Source folder does not exist.")
    input_vid_paths = [os.path.join(vid_source_dir, filename) for filename in os.listdir(vid_source_dir) if
                       filename.endswith(".mp4")]
    input_vid_paths = natsorted(input_vid_paths, key=lambda y: y.lower())

    # argument2: path to music
    if len(sys.argv) <= 2:
        error("No music path provided.")
    music_path = os.path.abspath(sys.argv[2])
    if not os.path.isfile(music_path):
        error(f"No valid music path: {music_path}")

    # argument3: path to yaml file with music timestamps
    if len(sys.argv) <= 3:
        error("No music timestamps provided.")
    timestamp_path = os.path.abspath(sys.argv[3])
    if not os.path.isfile(timestamp_path):
        error(f"No valid timestamp: {timestamp_path}")
    timestamp_list = yaml.safe_load(open(timestamp_path))  # read timestamps from yaml file
    # stop if not enough videos
    _timestamp_count_temp = len(timestamp_list)
    _video_count_temp = len(input_vid_paths)
    log.info(f"Amount of timestamps: {_timestamp_count_temp}, amount of videos: {_video_count_temp}.")
    if _timestamp_count_temp > _video_count_temp:
        error(f"Not enough videos for all the timestamps, {_timestamp_count_temp - _video_count_temp} more needed.")

    # argument4: path for temporary and result files
    if len(sys.argv) <= 4:
        error("No working folder provided.")
    working_path = os.path.abspath(sys.argv[4])
    if not os.path.isdir(working_path):
        os.mkdir(working_path)
        log.info(f"Created non-existing working directory: {working_path}")

    # argument5 (optional): steps to skip
    skip_steps_until = 0
    skip_steps_until = int(sys.argv[5]) if len(
        sys.argv) >= 6 else skip_steps_until  # use path passed as argument if one is given
    if skip_steps_until > 0:
        log.info(f"WARNING: skipping steps until step {skip_steps_until}.")


    def run_cmd(cmd):
        log.info(f"System call: {cmd}")
        try:
            cmd_output = subprocess.run(cmd, shell=True, check=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            cmd_output = cmd_output.stdout.decode()
            log.info(f"Process output: {cmd_output}")
        except subprocess.CalledProcessError as e:
            cmd_output = e.output.decode()
            log.error(f"Error process output: {cmd_output}")
            error(f"Error command:{cmd}\n{traceback.format_exc()}\n{cmd_output}\n"
                  f"Process failed with exit code {e.returncode}.")
        return cmd_output


    def ffmpeg_cmd(arguments_string):
        # wrapper function to make calls to ffmpeg
        cmd = f"ffmpeg -y {arguments_string}"  # -y: overwrite without asking
        return run_cmd(cmd)


    def create_folder_if_not_exists(path, msg):
        if os.path.isdir(path):
            return
        os.mkdir(path)
        log.info(f"Created directory for {msg}: {path}")


    # folder for intermediate video edits, for example trimmed videos, slowed down videos, etc
    temp_vids_dir = os.path.join(working_path, "temp_videos")
    create_folder_if_not_exists(temp_vids_dir, "temporary edited videos")
    # folder for input videos which are fully processed: have correct fps, overlay, proper trim, etc
    processed_vids_dir = os.path.join(working_path, "processed_videos")
    create_folder_if_not_exists(processed_vids_dir, "processed input videos")
    # folder for all the combined extracted frames from the processed videos
    extracted_frames_dir = os.path.join(working_path, "extracted_frames")
    create_folder_if_not_exists(extracted_frames_dir, "extracted frames")
    # folder for the exact frames of the end product
    result_frames_dir = os.path.join(working_path, "result_frames")
    create_folder_if_not_exists(result_frames_dir, "end product frames")
    # file with paths of frames for ffmpeg to construct the result video with
    result_vid_paths_txt_path = os.path.join(working_path, "result_vid_paths.txt")
    # file with log
    log_file_path = os.path.join(working_path, f"log_{runtime_id}.txt")
    # the result video file path
    result_vid_path = os.path.join(working_path, f"result_{runtime_id}.mp4")

    src_dir = os.path.join(os.getcwd(), "../src")
    font_path = os.path.join(src_dir, "font.ttf")
    if not os.path.isfile(font_path):  # I could also add just not to add text
        error(f"Cannot find file {font_path}")

    target_fps = 30
    slow_down_too_short_videos = True  # false: loops video instead
    trim_start_ms = 500  # trim too long videos starting from this timestamp, if there is enough room
    trim_slack = 1.2  # make input videos this times longer than they need to be, so we don't lack frames
    add_source_filename = True  # if false: use video number instead

    log.info(f"Editing input videos.")
    t = time.strftime("%H:%M:%S")
    print(f"{t}   Step 1 & 2: edit input videos.")
    previous_timestamp = 0
    video_number = 0
    for timestamp in timestamp_list:
        """
        Step 1:
        - make the input videos the desired length, plus a little extra.
        """
        t = time.strftime("%H:%M:%S")
        print(f"{t}   Editing input video {video_number} / {_timestamp_count_temp}", end="\r")
        # define these here if we were to skip step 1 but not 2
        input_vid_path = input_vid_paths[video_number]
        input_vid_filename = os.path.basename(input_vid_path)
        temp_vid_slowed_down_path = os.path.join(temp_vids_dir, f"video{video_number}_slowed_down.mp4")
        temp_vid_trimmed_path = os.path.join(temp_vids_dir, f"video{video_number}_trimmed.mp4")
        output_vid_path = os.path.join(processed_vids_dir, f"video{video_number}.mp4")
        if skip_steps_until >= 1:
            log.info(f"Skipping step 1!")
        else:
            required_length = timestamp - previous_timestamp
            required_length *= trim_slack
            previous_timestamp = timestamp

            # get approximate video length in ms
            cmd = f"ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 {input_vid_path}"
            input_vid_length = run_cmd(cmd)
            input_vid_length = input_vid_length.replace(".", "")
            input_vid_length = int(input_vid_length) // 1000
            duration_delta = input_vid_length - required_length
            log.info(f"{input_vid_filename}: duration={input_vid_length}ms, needs to be {required_length}ms.")

            # if duration is shorter than required duration, slow down the video to required duration
            if duration_delta < 0 and slow_down_too_short_videos:
                slow_factor = required_length / input_vid_length
                log.info(f"Slowing down {slow_factor}x.")
                ffmpeg_cmd(f"-i {input_vid_path} -filter:v 'setpts={slow_factor}*PTS' {temp_vid_slowed_down_path}")
                input_vid_path = temp_vid_slowed_down_path  # set the slowed vid as input for the next operation
            else:  # else trim the video to required length
                start_ms = trim_start_ms
                if input_vid_length - trim_start_ms < required_length:
                    # don't trim from later than the beginning of vid if this makes the trim result too short
                    log.info(
                        f"Trimming the video starting from {trim_start_ms} makes it too short, starting from 0 instead.")
                    start_ms = 0
                start_time_string = datetime.datetime.fromtimestamp(start_ms / 1000.0)
                start_time_string = start_time_string - datetime.timedelta(hours=1)
                start_time_string = start_time_string.strftime("%H:%M:%S.%f")
                end_time_string = datetime.datetime.fromtimestamp(required_length + start_ms / 1000.0)
                end_time_string = end_time_string - datetime.timedelta(hours=1)
                end_time_string = end_time_string.strftime("%H:%M:%S.%f")
                log.info(f"Trimming range: {start_time_string} to {end_time_string}.")
                ffmpeg_cmd(f"-ss {start_time_string} -to {end_time_string} "
                           f"-i {input_vid_path} -c copy {temp_vid_trimmed_path}")
                input_vid_path = temp_vid_trimmed_path  # set the trimmed vid as input for the next operation
            video_number += 1

        """
        Step 2: 
        - make videos all the same fps
        - put text overlay to quickly find the source video back from the end result
        """
        if skip_steps_until >= 2:
            log.info(f"Skipping step 2!")
        else:
            video_overlay_text = f"{video_number}" if not add_source_filename \
                else f"{video_number} - {input_vid_filename}"
            cmd = f"-i {input_vid_path} -vf " \
                  f"\"fps={target_fps},drawtext=fontfile={font_path}:" \
                  f"text='{video_overlay_text}':fontcolor=white:fontsize=24:box=1:" \
                  f"boxcolor=black@0.5:boxborderw=5:x=10:y=10\"" \
                  f" -codec:a copy {output_vid_path}"
            log.info(f"Putting text indicator '{video_overlay_text}' and setting fps to {target_fps}.")
            ffmpeg_cmd(cmd)

    """
    Step 3:
    - extract all the frames from the edited videos, to reconstruct our end result video from
    """
    t = time.strftime("%H:%M:%S")
    print(f"{t}   Step 3: extract frames from edited input videos.")
    if skip_steps_until >= 3:
        log.info(f"Skipping step 3!")
    else:
        log.info(f"Extracting frames from processed videos.")
        processed_vid_paths = [os.path.join(processed_vids_dir, filename) for filename in
                               os.listdir(processed_vids_dir)]
        processed_vid_paths = natsorted(processed_vid_paths, key=lambda y: y.lower())
        video_number = 0
        for trimmed_vid_path in processed_vid_paths:
            t = time.strftime("%H:%M:%S")
            print(f"{t}   Extracting frames from video {video_number} / {_timestamp_count_temp}", end="\r")
            cmd = f"-i {trimmed_vid_path} -qscale:v 2 {extracted_frames_dir}/video{video_number}_frame%03d.jpg"
            ffmpeg_cmd(cmd)
            video_number += 1

    """
    Step 4:
    - determine which frames go in the result video, so the timing is exactly as we want it
    """
    t = time.strftime("%H:%M:%S")
    print(f"{t}   Step 4: determining frames of result video.")
    if skip_steps_until >= 4:
        log.info(f"Skipping step 4!")
    else:
        log.info(f"Determining video frames of end product.")
        delay_between_frames = 1000 / target_fps  # 30 fps means 33.33 ms between each frame
        current_ms = 0  # the exact moment where we are with writing frames in our result video
        video_number = 0  # also the index of the timestamp list
        #  the timestamp we are currently writing frames towards,
        #  so when current_ms exceeds this timestamp, go write frames from the next video to the next timestamp
        current_timestamp = timestamp_list[video_number]
        current_video_frame_index = 0  # at which frame of the input video we are
        amount_of_source_frames = len(os.listdir(extracted_frames_dir))  # we need to loop a maximum am
        result_video_length = timestamp_list[-1]  # the music marker puts one timestamp at the exact end of the video
        writing_inverted = False  # set to true of no more frames, so we start writing frames backwards for loop effect
        amount_of_timestamps = len(timestamp_list)  # for stopping condition
        loop_count = 0  # add loop count to filenames, to keep correct order and against duplicate filenames in case of loop
        # keep looping until all frames are determined of result video
        # add little buffer so music does not get cut off, ffmpeg automatically trims this with --shortest or whatever
        while current_ms < result_video_length + 500:
            t = time.strftime("%H:%M:%S")
            print(f"{t}   Processed seconds of result video "
                  f"{round(current_ms // 1000)} / {(result_video_length + 500) // 1000}", end="\r")
            current_ms += delay_between_frames
            # when the total time of all the currently added frames exceeds this timestamp, go to the next video
            if current_ms > current_timestamp:
                log.info(f"Going to next video and timestamp.")
                video_number += 1
                if video_number >= len(timestamp_list):
                    log.info(f"All timestamps processed!")
                    break
                current_timestamp = timestamp_list[video_number]
                current_video_frame_index = 0
                writing_inverted = False

            # determine which frames in the folder belong to the current video
            regex_pattern = f".*?video{video_number}_frame\d\d\d.jpg"
            regex = re.compile(regex_pattern)
            vid_frames = [os.path.join(extracted_frames_dir, filename) for filename in os.listdir(extracted_frames_dir)
                          if
                          regex.match(filename)]
            vid_frames = natsorted(vid_frames, key=lambda y: y.lower())
            amount_of_vid_frames = len(vid_frames)
            if amount_of_vid_frames < 2:
                error(f"Aborted: video {regex_pattern} only has {amount_of_vid_frames}.")

            # loop the video backwards if no more frames to work with
            log.info(
                f"Current ms={current_ms}, video={video_number}, timestamp={current_timestamp}, frames={amount_of_vid_frames}")

            # move the frame to the result folder
            from_path = vid_frames[current_video_frame_index]
            # frame_filename = os.path.basename(path1)
            to_filename = f"{loop_count}_video{video_number}_frame{current_video_frame_index}.jpg"
            to_path = os.path.join(result_frames_dir, to_filename)
            log.info(f"Copying {from_path} to {to_path}")
            shutil.copy2(from_path, to_path)  # copy2 keeps metadata or whatever

            # handle input vids current frame index
            if not writing_inverted:
                current_video_frame_index += 1
            else:
                current_video_frame_index -= 1
            if current_video_frame_index >= amount_of_vid_frames and not writing_inverted:
                log.info(f"No more frames, looping backward!")
                writing_inverted = True
            if current_video_frame_index <= 0:
                log.info(f"No more frames, looping forwards again!")
                writing_inverted = False

            loop_count += 1

    """
    Step 5:
    - Construct the result video from our times frames and put the music under it.
    """

    t = time.strftime("%H:%M:%S")
    print(f"{t}   Step 5: creating result video from determined frames. "
          f"This takes 2 minutes on my above average computer.")
    if skip_steps_until >= 5:
        log.info(f"Skipping step 5!")
    else:
        log.info(f"Creating file with all the paths of the images needed to make the result video.")
        result_frames_paths = [os.path.join(result_frames_dir, filename) for filename in os.listdir(result_frames_dir)]
        result_frames_paths = natsorted(result_frames_paths, key=lambda y: y.lower())
        with open(result_vid_paths_txt_path, "w") as file:
            for result_frame_path in result_frames_paths:
                if not os.path.exists(result_frame_path):
                    # sanity check, result really should not miss any frames
                    error(f"Result frame path does not exist: {result_frame_path}")
                file.write(f"file {result_frame_path}\n")

        log.info(f"Generating video from frames.")
        cmd = f"-r {target_fps} -f concat -safe 0  -i {result_vid_paths_txt_path} -i {music_path} -c:a copy -shortest -c:v libx264 -pix_fmt yuv420p {result_vid_path} -y"
        ffmpeg_cmd(cmd)

    t = time.strftime("%H:%M:%S")
    print(f"{t}   Video compilation generated! Path: {result_vid_path}")

except KeyboardInterrupt:
    log.info(f"Program manually stopped!")
    print(f"Program manually stopped!")
