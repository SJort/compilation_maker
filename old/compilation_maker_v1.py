"""
Idea:
- loop video backwards to extend

Process:
- extend / trim video to time between timestamps with extra second buffer
- add visual marker
- set fps
- extract frames
- determine time per frame from fps
- determine the closest frame to timestamp

-an : remove audio from video
-y  : automatically say yes to overwrite etc

also: mediainfo --Output='General;%Duration%' 99_1651688441228534955.mp4 gives answer in ms

De video lijkt achter te lopen met de aangegeven beat.
Ik denk dat de videos niet exact worden getrimmed.
"""
import datetime
import os
import re
import shutil

import yaml
from natsort import natsorted

file = open("../src/output.yaml")
timestamps = yaml.safe_load(file)

root_path = "/home/jort/this/vid/compilation/"
concat_result_path = os.path.join(root_path, "concat_result.mp4")
result_path = os.path.join(root_path, "result.mp4")
vid_source_path = os.path.join(root_path, "test_vids")
vid_trimmed_path = os.path.join(root_path, "trimmed")
vid_frames_path = os.path.join(root_path, "frames")
vid_result_frames_path = os.path.join(root_path, "result_frames")
music_path = os.path.join(root_path, "../src/music.mp3")
font_path = os.path.join(root_path, "font.ttf")
source_vid_filenames = [os.path.join(vid_source_path, file) for file in os.listdir(vid_source_path)]
source_vid_filenames = natsorted(source_vid_filenames, key=lambda y: y.lower())
trimmed_filenames_txt_path = os.path.join(root_path, "filenames.txt")
print(f"Making compilation of {vid_source_path} using the following markers in {music_path}: {timestamps}")

count = 0
previous_timestamp = 0

trimmed_vid_paths = []

trim_videos = False
if trim_videos:
    print(f"Trimming videos.")
    for timestamp in timestamps:
        required_length = timestamp - previous_timestamp
        previous_timestamp = timestamp
        input_vid_path = source_vid_filenames[count]
        temp_vid_path_1 = os.path.join(vid_trimmed_path, f"{count}_temp1.mp4")
        temp_vid_path_2 = os.path.join(vid_trimmed_path, f"{count}_temp2.mp4")
        temp_vid_path_3 = os.path.join(vid_trimmed_path, f"{count}_temp3.mp4")
        trimmed_vid_path = os.path.join(vid_trimmed_path, f"{count}_trimmed.mp4")

        cmd = f"ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 {input_vid_path}"
        input_vid_length = os.popen(cmd).read()
        input_vid_length = input_vid_length.replace(".", "")
        input_vid_length = int(input_vid_length) // 1000
        duration_delta = input_vid_length - required_length
        print(f"{temp_vid_path_1} is {input_vid_length}ms, needs to be {required_length}ms (diff={duration_delta}ms).")

        if duration_delta < 0:  # if source is shorter than required length, slow down the video to that duration
            slow_factor = required_length / input_vid_length
            slow_factor *= 1.5  # add buffer to cut
            print(f"Slowing down {slow_factor}x.")
            os.system(f'ffmpeg -i {input_vid_path} -filter:v "setpts={slow_factor}*PTS" -an {temp_vid_path_3} -y')
            # now cut the video...
            time_string = datetime.datetime.fromtimestamp(required_length / 1000.0)
            time_string = time_string - datetime.timedelta(hours=1)
            time_string = time_string + datetime.timedelta(seconds=1)  # add as buffer
            time_string = time_string.strftime("%H:%M:%S.%f")
            cmd = f"ffmpeg -ss 00:00:00.000 -to {time_string} -i {temp_vid_path_3} -c copy -an {temp_vid_path_1} -y"
            print(f"SLOWMO TRIM CMD: {cmd}")
            os.system(cmd)
        else:  # else trim the video to required length
            time_string = datetime.datetime.fromtimestamp(required_length / 1000.0)
            time_string = time_string - datetime.timedelta(hours=1)
            time_string = time_string + datetime.timedelta(seconds=1)  # add as buffer
            time_string = time_string.strftime("%H:%M:%S.%f")
            cmd = f"ffmpeg -ss 00:00:00.000 -to {time_string} -i {input_vid_path} -c copy -an {temp_vid_path_1} -y"
            print(f"TRIM cmd: {cmd}")
            os.system(cmd)

        text = f"#{count}\ntimestamp ms:{timestamp}"
        print(f"Putting text indicator: {text}")
        cmd = f"ffmpeg -i {temp_vid_path_1} -vf " \
              f"\"drawtext=fontfile={font_path}:text='{text}':fontcolor=white:fontsize=24:box=1:" \
              f"boxcolor=black@0.5:boxborderw=5:x=10:y=10\"" \
              f" -codec:a copy {temp_vid_path_2} -y"
        print(f"CMD: {cmd}")
        os.system(cmd)

        print(f"Setting frame rate to 30.")  # otherwise concat gives weird results
        os.system(f"ffmpeg -i {temp_vid_path_2} -filter:v fps=30 {trimmed_vid_path} -y")
        # os.remove(temp_vid_path_1) if os.path.exists(temp_vid_path_1) else None
        # os.remove(temp_vid_path_2) if os.path.exists(temp_vid_path_2) else None
        trimmed_vid_paths.append(trimmed_vid_path)

        count += 1
        # if count > 10:
        #     break
else:
    print(f"Using old trimmed videos.")
    trimmed_vid_paths = [os.path.join(vid_trimmed_path, file) for file in os.listdir(vid_trimmed_path) if "trimmed" in file]
    trimmed_vid_paths = natsorted(trimmed_vid_paths, key=lambda y: y.lower())

print(f"Trimmed video paths: {trimmed_vid_paths}")
print(f"Extracting frames.")
count = 0
for trimmed_vid_path in trimmed_vid_paths:
    cmd = f"ffmpeg -i {trimmed_vid_path} -qscale:v 2 {vid_frames_path}/s{count}e%03d.jpg"
    print(f"{cmd}")
    os.system(cmd)
    count += 1

fps = 30
tpf = 1000 / fps

print(f"Determining video frames.")
current_ms = 0
current_timestamp_index = 0
current_timestamp = timestamps[current_timestamp_index]
count = 0
current_timestamp_cycle_index = 0
for x in range(len(os.listdir(vid_frames_path))):
    current_ms += tpf
    # when the total time of all the currently added frames exceeds this timestamp, go to the next
    if current_ms > current_timestamp:
        print(f"Going to next timestamp.")
        current_timestamp_index += 1
        if current_timestamp_index >= len(timestamps):
            print(f"All timestamps processed.")
            break
        current_timestamp = timestamps[current_timestamp_index]
        current_timestamp_cycle_index = 0

    # move the first following frame to the result folder
    regex = re.compile(f".*?s{current_timestamp_index}e\d\d\d.jpg")
    frames = [os.path.join(vid_frames_path, file) for file in os.listdir(vid_frames_path) if regex.match(file)]
    frames = natsorted(frames, key=lambda y: y.lower())

    print(f"Current ms: {current_ms}, timestamp_index: {current_timestamp_index}, timestamp: {current_timestamp}, frames: {len(frames)}")
    if len(frames) <= current_timestamp_cycle_index:
        print(f"No more frames!")
        break
    path1 = frames[current_timestamp_cycle_index]
    # frame_filename = os.path.basename(path1)
    frame_filename = f"{count}_{current_timestamp_index}.jpg"
    current_timestamp_cycle_index += 1

    path2 = os.path.join(vid_result_frames_path, frame_filename)
    print(f"Copying {path1} to {path2}")
    shutil.copy2(path1, path2)
    count += 1


print(f"Creating txt file with all the paths of the images needed to make the result video.")
result_frames = [os.path.join(vid_result_frames_path, file) for file in os.listdir(vid_result_frames_path)]
result_frames = natsorted(result_frames, key=lambda y: y.lower())
with open(trimmed_filenames_txt_path, "w") as file:
    for result_frame_path in result_frames:
        if os.path.exists(result_frame_path):
            file.write(f"file {result_frame_path}\n")
        else:
            print(f"DOESNOTEXIST: {result_frame_path}")
            quit()

print(f"Generating video from frames.")
cmd = f"ffmpeg -r 30 -f concat -safe 0  -i {trimmed_filenames_txt_path} -i {music_path} -c:a copy -shortest -c:v libx264 -pix_fmt yuv420p {result_path} -y"
os.system(cmd)
print(f"Done!")

quit()

print(f"Creating file with trimmed video paths for ffmpeg.")
with open(trimmed_filenames_txt_path, "w") as file:
    for trimmed_vid in trimmed_vid_paths:
        if os.path.exists(trimmed_vid):
            file.write(f"file {trimmed_vid}\n")
        else:
            print(f"DOESNOTEXIST: {trimmed_vid}")

print(f"Concatenating trimmed videos.")
os.system(f"ffmpeg -f concat -safe 0 -i {trimmed_filenames_txt_path} -c copy {concat_result_path} -y")

print(f"Adding audio track to concatenated video.")
os.system(f"ffmpeg -i {concat_result_path} -i {music_path} -map 0:v -map 1:a -c:v copy -shortest {result_path} -y")
print(f"Done! {result_path}")
