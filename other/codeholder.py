import logging
import re

import cv2
from natsort import natsorted

import os
import file_manipulation as fm
import general_functions as gf

log = logging.getLogger("result_frames")


def get_video_frames(video_number, source_frames_dir):
    regex_pattern = f".*?video{video_number}_frame\d*.jpg"
    regex = re.compile(regex_pattern)
    vid_frame_paths = [os.path.join(source_frames_dir, filename) for filename in os.listdir(source_frames_dir)
                       if regex.match(filename)]
    vid_frame_paths = natsorted(vid_frame_paths, key=lambda y: y.lower())
    return vid_frame_paths


def determine_result_frames(source_frames_dir, result_frames_dir, music_timestamps, timestamps_per_video,
                            video_data_list, target_fps):
    gf.print_log(f"Determining frames of result video.")
    log.info(f"Determining video frames of end product.")
    fm.realise_dir(result_frames_dir, "frames to create the result video from")
    delay_between_frames = 1000 / target_fps  # 30 fps means 33.33 ms between each frame
    current_ms = 0  # the exact moment where we are with writing frames in our result video
    video_number = 0  # also the index of the timestamp list
    #  the timestamp we are currently writing frames towards,
    #  so when current_ms exceeds this timestamp, go write frames from the next video to the next timestamp
    current_timestamp = music_timestamps[video_number]
    current_video_frame_index = 0  # at which frame of the input video we are
    result_video_length = music_timestamps[-1]  # the music marker puts one timestamp at the exact end of the video
    # add frame count to filenames, to keep correct order and against duplicate filenames in case of video loop
    current_video_ms = 0  # ms within the current video being handled
    frame_count = 0  # also the amount of times looped

    # determine which frames in the folder belong to the first video
    # gets updated in the loop when iterating to next video
    video_number = 0
    timestamp_index = 0  # moves faster than videos, since multiple timestamps are used in 1 video clip
    vid_frames = get_video_frames(video_number, source_frames_dir)
    amount_of_vid_frames = len(vid_frames)

    is_frozen = False
    freeze_count = 0
    freeze_until = 0

    is_first_frame = True
    has_been_frozen = False
    has_incremented = False
    has_been_slowmo = False
    slowmo_until = 0

    is_slowmo = False

    kill_count = 0
    dead_count = 0

    while current_ms < result_video_length + 500:
        gf.print_log(f"Processing {round(current_ms // 1000)} / {(result_video_length + 500) // 1000}"
                     f" of result video", end="\r")

        if len(music_timestamps) <= timestamp_index + timestamps_per_video + 2:
            log.info("End of timestamp data.")
            break

        if len(video_data_list) < video_number + 1:
            log.info("End of highlight data reached.")
            break

        if current_ms > music_timestamps[timestamp_index + timestamps_per_video]:
            log.info(f"Going to next video and next {timestamps_per_video} timestamps.")
            video_number += 1
            current_video_frame_index = 0
            current_video_ms = 0
            timestamp_index += timestamps_per_video
            freeze_count = 0
            is_frozen = False
            is_slowmo = False
            has_been_slowmo = False
            freeze_until = 0
            slowmo_until = 0
            has_been_frozen = False
            is_first_frame = True
            has_incremented = False

            # determine which frames in the folder belong to the current video
            vid_frames = get_video_frames(video_number, source_frames_dir)
            amount_of_vid_frames = len(vid_frames)

        log.info(f"{current_ms=}, {video_number=}, {timestamp_index=}, {amount_of_vid_frames=}, "
                 f"{current_video_frame_index=}, {current_video_ms=}")

        if amount_of_vid_frames < 2:
            # this case can happen when there are less videos than timestamps
            log.warning(f"Not enough frames in video! Going to next video.")
            video_number += 1
            continue

        # at which music timestamp the highlight should happen
        highlight_timestamp_index = timestamp_index + timestamps_per_video // 2 + 2
        highlight_timestamp = music_timestamps[highlight_timestamp_index]
        # how many ms that timestamp is after the current timestamp
        highlight_timestamp_delta = highlight_timestamp - music_timestamps[timestamp_index]
        # how many ms in the current video the highlight happens
        current_video_highlight_ms = round(video_data_list[video_number]["highlight"] * 1000)
        # how many ms the current video may play until highlight
        current_video_prefix_trim = current_video_highlight_ms - highlight_timestamp_delta
        log.info(f"{highlight_timestamp=}, {highlight_timestamp_delta=}, {current_video_highlight_ms=}, "
                 f"{current_video_prefix_trim=}")

        if current_video_prefix_trim < 0:
            log.warning("Input video too short, going to next video.")
            video_number += 1
            continue

        if current_video_ms < current_video_prefix_trim:
            log.info("Skipping frame.")
            current_video_ms += delay_between_frames
            current_video_frame_index += 1
            continue

        # freeze condition: highlight
        # if current_video_highlight_ms < current_video_ms and not is_frozen and not has_been_frozen:
        #     # freeze until next highlight
        #     freeze_duration = music_timestamps[highlight_timestamp_index + 1] - music_timestamps[
        #         highlight_timestamp_index]
        #     freeze_until = current_ms + freeze_duration
        #     is_frozen = True
        #     has_been_frozen = True
        #     log.info(f"Freezing for {freeze_duration} until {freeze_until}!")

        # slow motion condition: before and after highlight
        slowmo_pre_timestamps = 3
        slowmo_timestamp_duration = 2
        diff = music_timestamps[highlight_timestamp_index] - music_timestamps[highlight_timestamp_index - 3]
        slow_motion_trigger_ms = current_video_highlight_ms - diff
        if slow_motion_trigger_ms < current_video_ms and not is_slowmo and not has_been_slowmo:
            slowmo_duration = music_timestamps[highlight_timestamp_index + slowmo_timestamp_duration] - \
                              music_timestamps[
                                  highlight_timestamp_index]
            log.info("Slow motion triggered!")
            slowmo_until = current_ms + slowmo_duration
            is_slowmo = True
            has_been_slowmo = True

        if is_slowmo and slowmo_until < current_ms:
            log.info("Undoing slow motion!")
            is_slowmo = False

        # unfreeze if freeze timer stopped
        if is_frozen and freeze_until < current_ms:
            log.info("Unfreezing!")
            is_frozen = False

        # write new frame to result video
        if len(vid_frames) <= int(current_video_frame_index) + 1:
            log.warning("Video seems too short, using last frame!")
            current_video_frame_index = len(vid_frames) - 1
        from_path = vid_frames[int(current_video_frame_index)]
        to_filename = f"{frame_count}_video{video_number}_frame{current_video_frame_index}.jpg"
        to_path = os.path.join(result_frames_dir, to_filename)
        log.info(f"Copying {from_path} to {to_path}")
        if is_first_frame:
            black_image = np.zeros((resolution_height, resolution_width, 3), np.uint8)
            is_first_frame = False
            cv2.imwrite(to_path, black_image)
        else:
            shutil.copy2(from_path, to_path)  # copy2 keeps metadata or something

        for timestamp in music_timestamps:
            # doing it a bit before the timestamp feels better
            if current_ms - delay_between_frames * 2 <= timestamp <= current_ms and not is_slowmo:
                log.info("Editing image on beat.")
                img = cv2.imread(to_path)
                img = im.increase_brightness(img, 5)
                cv2.imwrite(to_path, img)

        if is_frozen or is_slowmo:
            log.info("Editing written frozen/slowmo frame.")
            if not has_incremented:
                kill_count += 1
                has_incremented = True
            text = f"Kill {kill_count}"
            img = cv2.imread(to_path)
            font = cv2.FONT_HERSHEY_SIMPLEX
            cv2.putText(img=img, text=text, org=(360, 110), fontFace=font,
                        fontScale=3, color=(0, 255, 255), thickness=2, lineType=cv2.LINE_AA)
            # cv2.putText(img=img, text=f"Clip: {filename}", org=(10, 30), fontFace=font,
            #             fontScale=1, color=(0, 255, 255), thickness=1, lineType=cv2.LINE_AA)
            img = im.increase_brightness(img, 30)
            cv2.imwrite(to_path, img)

        if not is_frozen:
            current_video_ms += delay_between_frames
            if is_slowmo:
                current_video_frame_index += 0.5
            else:
                current_video_frame_index += 1

        current_ms += delay_between_frames
        frame_count += 1

        if debug:
            # # rendering preview
            # rendering_preview_img = cv2.imread(to_path)
            # # rendering_preview_img = cv2.resize(rendering_preview_img, (720, 480))
            # cv2.imshow("Rendering preview", rendering_preview_img)
            # cv2.waitKey(1)

            # early stopping
            if frame_count > 500:
                print(f"Test stop!")
                break

    cv2.destroyAllWindows()
    amount = len(
        [name for name in os.listdir(result_frames_dir) if os.path.isfile(os.path.join(result_frames_dir, name))])
    gf.print_log(f"Determined {amount} frames for result video!")
