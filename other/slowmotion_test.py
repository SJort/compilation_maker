#!/usr/bin/env python3

import datetime
import logging
import os
import shutil
import subprocess
import sys
import time
import traceback

import cv2
import yaml
from natsort import natsorted

runtime_id = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
log_file_path = f"../log/test_log.txt"
# log_file_path = f"log/compilation_maker_log"
open(log_file_path, "w").close()
# write logs to file, and to stream (just log.info them), log everything with severity of debug and higher like info
# logging.basicConfig(handlers=[logging.FileHandler(log_file_path), logging.StreamHandler()], level=logging.DEBUG)
logging.basicConfig(handlers=[logging.FileHandler(log_file_path)], level=logging.DEBUG)
log = logging.getLogger("test")


def print_log(msg, end="\n"):
    print(f"{time.strftime('%H:%M:%S')}   {msg}", end=end)
    log.info(msg)


def error(msg):
    log.critical(msg)
    print(msg)
    quit(1)


def run_cmd(cmd):
    log.info(f"System call: {cmd}")
    try:
        cmd_output = subprocess.run(cmd, shell=True, check=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        cmd_output = cmd_output.stdout.decode()
        log.info(f"Process output: {cmd_output}")
    except subprocess.CalledProcessError as e:
        cmd_output = e.output.decode()
        log.error(f"Error process output: {cmd_output}")
        error(f"Error command:{cmd}\n{traceback.format_exc()}\n{cmd_output}\n"
              f"Process failed with exit code {e.returncode}.")
    return cmd_output


def ffmpeg_cmd(arguments_string):
    # wrapper function to make calls to ffmpeg
    cmd = f"ffmpeg -y {arguments_string}"  # -y: overwrite without asking
    return run_cmd(cmd)


def realise_dir(path, msg="intermediate process results"):
    # makes sure the directory exists and is empty
    if os.path.isdir(path):
        log.info(f"Removed existing directory for {msg}: {path}")
        shutil.rmtree(path)  # faster than removing each file individually
    log.info(f"Created directory for {msg}: {path}")
    os.mkdir(path)


def require_dir(path):
    if not os.path.isdir(path):
        error(f"Directory does not exist: {path}")
    if len(os.listdir(path)) == 0:
        error(f"Directory is empty: {path}")


def increase_brightness(img, value=30):
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    h, s, v = cv2.split(hsv)

    lim = 255 - value
    v[v > lim] = 255
    v[v <= lim] += value

    final_hsv = cv2.merge((h, s, v))
    img = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)
    return img


try:
    # adjustable variables which cannot be set by command line arguments (yet)
    target_fps = 60  # fps of end result, frames get duplicated if input is less (which means it does make it smoother)
    # resolution of result. inputs get scaled and cropped to this width. recommended to set the same as your input vids
    resolution_width = 1920
    resolution_height = 1080
    timestamps_per_video = 10
    debug = False
    skip_steps_until = int(sys.argv[1]) if len(sys.argv) >= 2 else 0
    video_extension = "mkv"
    working_dir = "/home/jort/this/vid/osrs"

    # source highlights dir
    source_highlights_dir = os.path.join(working_dir, "highlight_files")
    require_dir(source_highlights_dir)
    video_data_list = []
    log.info("Reading highlight data.")

    for filename in natsorted(os.listdir(source_highlights_dir), key=lambda y: y.lower()):
        path = os.path.join(source_highlights_dir, filename)
        video_data = yaml.safe_load(open(path))
        video_data = {"filename": video_data["mediaFileName"], "highlight": video_data["cutSegments"][0]["start"]}
        video_data_list.append(video_data)

    # source video dir
    source_vids_dir = os.path.join(working_dir, "source_videos")
    require_dir(source_vids_dir)
    log.info("Checking if videos associated with highlight data exist.")
    video_filenames = os.listdir(source_vids_dir)
    for d in video_data_list:
        if d["filename"] not in video_filenames:
            error(f"No video found with highlight: {d['filename']}")

    # source music file
    music_path = os.path.join(working_dir, "showdown.mp3")
    if not os.path.isfile(music_path):
        error(f"No valid music path: {music_path}")

    # source timestamps file
    timestamp_path = os.path.join(working_dir, "showdown_timestamps.yaml")
    if not os.path.isfile(timestamp_path):
        error(f"No valid timestamp path: {timestamp_path}")
    timestamp_dict = yaml.safe_load(open(timestamp_path))  # read timestamps from yaml file
    type1_timestamps = timestamp_dict["type1"]
    print_log(f"Using {len(type1_timestamps)} type1 timestamps.")

    # result directories
    extracted_frames_dir = os.path.join(working_dir, "extracted_frames")
    result_frames_dir = os.path.join(working_dir, "result_frames")

    # result video frames txt file for ffmpeg to construct video with
    result_vid_paths_txt_path = os.path.join(working_dir, "result_vid_paths.txt")
    # result video path
    result_vid_path = os.path.join(working_dir, f"result_{runtime_id}.{video_extension}")

    ####################################################################################################################
    step = 1
    print_log(f"Step {step}: Determine result frames.")
    if skip_steps_until >= step:
        print_log(f"Skipping step {step}!")
    else:
        frame_filenames = natsorted(os.listdir(extracted_frames_dir), key=lambda y: y.lower())
        frame_paths = [os.path.join(extracted_frames_dir, filename) for filename in frame_filenames]
        realise_dir(result_frames_dir)

        min_slowmo = 1.5
        max_slowmo = 3
        current_slowmo = min_slowmo

        slowmo_start = 1000
        slowmo_end = 4000
        result_video_length = 5000
        slowmo_count = 0
        slowmo_ms = 0

        current_ms = 0
        delay_between_frames = 1000 / target_fps
        frame_index = 0
        frame_count = 0
        slowmo = False

        # determine which frame to write according to current_ms
        while current_ms <= result_video_length:
            from_path = frame_paths[int(frame_index)]
            print_log(f"Processed second {round(current_ms // 1000)} / {round(result_video_length // 1000)}"
                      f" of result video.", end="\r")
            to_filename = f"frame{frame_count}.jpg"
            to_path = os.path.join(result_frames_dir, to_filename)
            log.info(f"Copying {from_path} to {to_path}")
            shutil.copy2(from_path, to_path)  # copy2 keeps metadata or something

            if slowmo_start <= current_ms <= slowmo_end:
                slowmo = True
            else:
                slowmo = False

            if slowmo:
                log.info("Slow motion frame.")
                text = f"Slowmotion #{slowmo_count}"
                slowmo_count += 1
                img = cv2.imread(to_path)
                img = increase_brightness(img, 50)
                font = cv2.FONT_HERSHEY_SIMPLEX
                cv2.putText(img=img, text=text, org=(10, 30), fontFace=font,
                            fontScale=1, color=(0, 255, 255), thickness=1, lineType=cv2.LINE_AA)
                cv2.putText(img=img, text=f"Slow motion {round(current_slowmo / 1)}", org=(400, 100), fontFace=font,
                            fontScale=3, color=(0, 255, 255), thickness=2, lineType=cv2.LINE_AA)
                cv2.imwrite(to_path, img)
                frame_index += 1 / current_slowmo
                if current_slowmo < max_slowmo:
                    current_slowmo += 0.01

            else:
                log.info("Normal frame.")
                frame_index += 1

            current_ms += delay_between_frames
            frame_count += 1
            log.info(f"Iteration #{frame_count} {frame_index=}, {current_ms=}")

    step = 2
    print_log(f"Step {step}: creating result video from determined frames.")
    if skip_steps_until >= step:
        print_log(f"Skipping step {step}!")
    else:
        log.info(f"Creating file with all the paths of the images needed to make the result video.")
        result_frames_paths = [os.path.join(result_frames_dir, filename) for filename in os.listdir(result_frames_dir)]
        result_frames_paths = natsorted(result_frames_paths, key=lambda y: y.lower())
        with open(result_vid_paths_txt_path, "w") as file:
            for result_frame_path in result_frames_paths:
                if not os.path.exists(result_frame_path):
                    # sanity check, result really should not miss any frames
                    error(f"Result frame path does not exist: {result_frame_path}")
                file.write(f"file {result_frame_path}\n")

        log.info(f"Generating video from frames.")
        cmd = f"-r {target_fps} -f concat -safe 0  -i {result_vid_paths_txt_path} -i {music_path} " \
              f"-c:a copy -shortest -c:v libx264 -pix_fmt yuv420p {result_vid_path}"
        ffmpeg_cmd(cmd)

    print_log(f"Video compilation generated! Path: {result_vid_path}")

except KeyboardInterrupt:
    print()
    print_log(f"Program manually stopped!")
