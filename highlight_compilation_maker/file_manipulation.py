import logging
import os
import shutil
import general_functions as gf
from natsort import natsorted

log = logging.getLogger("compilation_maker")


def get_sorted_paths(dir):
    """
    :param dir: The directory with files to sort.
    :return: A list of absolute paths sorted like the file manager.
    """
    paths = [os.path.join(dir, filename) for filename in os.listdir(dir)]
    sorted_paths = natsorted(paths, key=lambda y: y.lower())
    return sorted_paths

def realise_dir(path, msg):
    # makes sure the directory exists and is empty
    if os.path.isdir(path):
        log.info(f"Removed existing directory for {msg}: {path}")
        shutil.rmtree(path)  # faster than removing each file individually
    log.info(f"Created directory for {msg}: {path}")
    os.mkdir(path)


def require_dir(path):
    if not os.path.isdir(path):
        gf.error(f"Directory does not exist: {path}")
    if len(os.listdir(path)) == 0:
        gf.error(f"Directory is empty: {path}")
