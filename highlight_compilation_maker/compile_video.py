import logging
import os

from natsort import natsorted
import general_functions as gf

log = logging.getLogger("compile_video")


def compile_video(frames_dir, frames_txt_path, output_video_path, target_fps, music_path):
    gf.print_log("Generating result video from frames.")
    log.info(f"Creating file with all the paths of the images needed to make the result video.")
    result_frames_paths = [os.path.join(frames_dir, filename) for filename in os.listdir(frames_dir)]
    result_frames_paths = natsorted(result_frames_paths, key=lambda y: y.lower())
    with open(frames_txt_path, "w") as file:
        for result_frame_path in result_frames_paths:
            if not os.path.exists(result_frame_path):
                # sanity check, result should not miss any frames for sync to be correct
                gf.error(f"Result frame path does not exist: {result_frame_path}")
            file.write(f"file {result_frame_path}\n")

    cmd = f"-r {target_fps} -f concat -safe 0  -i {frames_txt_path} -i {music_path} " \
          f"-c:a copy -shortest -c:v libx264 -pix_fmt yuv420p {output_video_path}"
    gf.ffmpeg_cmd(cmd)
    gf.print_log(f"Video generated! Location: {output_video_path}")
