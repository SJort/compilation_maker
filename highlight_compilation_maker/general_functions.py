import logging
import subprocess
import time
import traceback

log = logging.getLogger("compilation_maker")


def print_log(msg, end="\n"):
    print(f"{time.strftime('%H:%M:%S')}   {msg}", end=end)
    log.info(msg)


def error(msg):
    log.critical(msg)
    print(f"Error, program stopped: {msg}")
    quit(1)


def run_cmd(cmd):
    log.info(f"System call: {cmd}")
    try:
        cmd_output = subprocess.run(cmd, shell=True, check=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        cmd_output = cmd_output.stdout.decode()
        log.info(f"Process output: {cmd_output}")
    except subprocess.CalledProcessError as e:
        cmd_output = e.output.decode()
        log.error(f"Error process output: {cmd_output}")
        error(f"Error command:{cmd}\n{traceback.format_exc()}\n{cmd_output}\n"
              f"Process failed with exit code {e.returncode}.")
    return cmd_output


def ffmpeg_cmd(arguments_string):
    # wrapper function to make calls to ffmpeg
    cmd = f"ffmpeg -y {arguments_string}"  # -y: overwrite without asking
    return run_cmd(cmd)
