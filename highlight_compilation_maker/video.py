"""
Class for holding data of one input video used to create the compilation with.
"""


class Video:
    def __init__(self, index: int):
        """
        Class for video. None parameters should be manually be assigned once.
        :param index: Index of the video, used to determine which frames belong to it.
        """
        self.index = index
        self.filename = None
        self.source_path = None
        self.frame_index = 0.0  # index of the frame currently being read (can be fraction for slow motion)
        self.read_ms = 0  # in-video ms of the frame currently being read
        self.highlight_timestamp = None  # ms within this video the highlight happens
        # ms within this video the highlight special effect should be started and stopped
        self.highlight_effect_start_ms = None
        self.highlight_effect_stop_ms = None
        self.effect_start_ms = None
        self.effect_stop_ms = None
        # how many ms should be trimmed off the beginning of this video to
        # have the highlight timestamp sync with the beat
        self.discard_ms = None
        self.duration_ms = None  # calculated with amount of frame paths and fps
        self.frame_paths = None  # the paths of all the extracted frames which belong to this video

    def __repr__(self):
        # return f"Video: {str(self.__dict__)}"
        dic = {key: self.__dict__[key] for key in self.__dict__ if key not in ["frame_paths"]}
        return f"Video: {dic}"
