import logging
import os
import re

import file_manipulation as fm
import general_functions as gf
from video import Video
import yaml
from natsort import natsorted

log = logging.getLogger("video_data")


def get_video_list(video_dir, highlight_dir):
    log.info(f"Determining video data. {video_dir=}, {highlight_dir=}")
    video_list = []
    index = 0
    highlight_paths = fm.get_sorted_paths(highlight_dir)
    gf.print_log(f"Reading metadata of {len(highlight_paths)} videos.")
    for highlight_path in highlight_paths:
        try:
            gf.print_log(f"Reading video {index} / {len(highlight_paths) - 1}", end="\r")
            video = Video(index=index)
            index += 1
            highlight_data = yaml.safe_load(open(highlight_path))
            video.highlight_timestamp = round(highlight_data["cutSegments"][0]["start"] * 1000)
            video.filename = highlight_data["mediaFileName"]
            video.source_path = os.path.join(video_dir, video.filename)
            if not os.path.isfile(video.source_path):
                raise FileNotFoundError(f"Highlight referred to non-existing video: {video.source_path}")

            video_list.append(video)
            log.info(f"Read video: {video}")
        except Exception as e:
            log.warning(f"Error reading video {index}:")
            log.exception(e)
            continue
    if len(video_list) == 0:
        gf.error("\nNo videos read.")
    gf.print_log(f"\nRead {index} videos!")
    return video_list


def add_frames_to_video_list(video_list, frames_dir, target_fps):
    frame_paths = fm.get_sorted_paths(frames_dir)
    gf.print_log(f"Determining which frames out of {len(frame_paths)} frames belong to each video.")
    for video in video_list:
        gf.print_log(f"Determining frames for video {video.index} / {len(video_list)}", end="\r")
        regex_pattern = f".*?video{video.index}_frame\d*.jpg"
        log.info(f"Regex pattern: {regex_pattern}")
        regex = re.compile(regex_pattern)
        video_frame_paths = [frame_path for frame_path in frame_paths if regex.match(frame_path)]
        # video_frame_paths = [frame_path for frame_path in frame_paths if f"video{video.index}_frame" in frame_path]
        log.info(f"Updating frames of video {video}")
        frame_ms = 1000 / target_fps  # 60 fps means 16.67 ms between each frame
        video.duration_ms = int(frame_ms * len(video_frame_paths))
        video.frame_paths = video_frame_paths
        # log.info(f"Video frames are now {video.frame_paths}")
        if len(video.frame_paths) < 2:
            gf.error(f"Not enough frames in {video}")

    gf.print_log(f"\nDetermined frames for {len(video_list)} videos!")
