import logging


import os
import file_manipulation as fm
import general_functions as gf

log = logging.getLogger("frame_extractor")


def extract_frames(video_list, result_frames_dir):
    # clear / create result directory
    fm.realise_dir(result_frames_dir, "extracted frames of all videos")

    gf.print_log(f"Extracting frames from {len(video_list)} videos.")
    video_index = 0
    for video in video_list:
        # could have: add fancy name parameter to video object
        gf.print_log(f"Extracting frames from video {video_index} / {len(video_list)}", end="\r")
        cmd = f"ffmpeg -i {video.source_path} -q:v 1 {result_frames_dir}/video{video_index}_frame%d.jpg"
        video_index += 1
        gf.run_cmd(cmd)

    amount = len([name for name in os.listdir(result_frames_dir) if os.path.isfile(os.path.join(result_frames_dir, name))])
    gf.print_log(f"\nExtracted {amount} frames from {video_index} videos!")
