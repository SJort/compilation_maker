#!/usr/bin/env python3
"""
Create a compilation of clips with each one highlight on the beat of music.

goal: 2/3 clip before kill, kill, 1/3 clip after kill, next clip, repeat
beat0           beat1                   beat2                 beat3                         beat4                 beat0
next clip       clip playing (1s)       clip playing (2s)     kill, video effects (3s)      clip playing (4s)     (5s) next clip
"""

import datetime
import logging
import os
import sys

import yaml

import determine_result_frames
import file_manipulation as fm
import frame_extractor
import general_functions as gf
from compile_video import compile_video
from set_video_data import get_video_list

runtime_id = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
working_dir = "/home/jort/this/vid/osrs"
# log_file_path = os.path.join(working_dir, f"highlight_compilation_maker_log.txt")
log_file_path = f"highlight_compilation_maker_log.txt"
open(log_file_path, "w").close()
logging.basicConfig(handlers=[logging.FileHandler(log_file_path)], level=logging.DEBUG)
log = logging.getLogger("compilation_maker")

try:
    # adjustable variables which cannot be set by command line arguments (yet)
    target_fps = 60  # fps of end result, frames get duplicated if input is less (which means it does make it smoother)
    # resolution of result. inputs get scaled and cropped to this width. recommended to set the same as your input vids
    resolution_width = 1920
    resolution_height = 1080
    debug = 0
    skip_steps_until = int(sys.argv[1]) if len(sys.argv) >= 2 else 0
    video_extension = "mkv"

    # source highlights dir
    source_highlights_dir = os.path.join(working_dir, "highlight_files")
    fm.require_dir(source_highlights_dir)

    # source video dir
    source_vids_dir = os.path.join(working_dir, "source_videos")
    fm.require_dir(source_vids_dir)

    video_list = get_video_list(video_dir=source_vids_dir, highlight_dir=source_highlights_dir)

    # source music file
    music_path = os.path.join(working_dir, "music.mp3")
    if not os.path.isfile(music_path):
        gf.error(f"No valid music path: {music_path}")

    # source timestamps file
    timestamp_path = os.path.join(working_dir, "music_timestamps.yaml")
    if not os.path.isfile(timestamp_path):
        gf.error(f"No valid timestamp path: {timestamp_path}")
    timestamp_dict = yaml.safe_load(open(timestamp_path))  # read timestamps from yaml file
    type1_timestamps = timestamp_dict["type1"]
    gf.print_log(f"Using {len(type1_timestamps)} music timestamps.")

    # result directories
    extracted_frames_dir = os.path.join(working_dir, "extracted_frames")
    result_frames_dir = os.path.join(working_dir, "result_frames")

    # result video frames txt file for ffmpeg to construct video with
    result_vid_paths_txt_path = os.path.join(working_dir, "result_vid_paths.txt")
    # result video path
    result_vid_path = os.path.join(working_dir, f"result_{runtime_id}.{video_extension}")

    ####################################################################################################################
    step = 1
    if skip_steps_until >= step:
        gf.print_log(f"Skipping step {step}!")
    else:
        frame_extractor.extract_frames(video_list=video_list, result_frames_dir=extracted_frames_dir)
    ####################################################################################################################
    step = 2
    if skip_steps_until >= step:
        gf.print_log(f"Skipping step {step}!")
    else:
        determine_result_frames.determine_result_frames(source_frames_dir=extracted_frames_dir,
                                                        result_frames_dir=result_frames_dir,
                                                        music_timestamps=type1_timestamps,
                                                        timestamps_per_video=10,
                                                        highlight_timestamp_after_index=6,
                                                        video_list=video_list,
                                                        target_fps=target_fps)
    ####################################################################################################################
    step = 3
    if skip_steps_until >= step:
        gf.print_log(f"Skipping step {step}!")
    else:
        compile_video(frames_dir=result_frames_dir, frames_txt_path=result_vid_paths_txt_path,
                      output_video_path=result_vid_path, target_fps=target_fps, music_path=music_path)

except KeyboardInterrupt:
    gf.print_log(f"\nProgram manually stopped!")
