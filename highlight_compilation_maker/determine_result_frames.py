import logging
import os
import re
import shutil

import cv2
from PIL import Image
from natsort import natsorted

import file_manipulation as fm
import general_functions as gf
from video import Video
from set_video_data import add_frames_to_video_list
import image_manipulation as im

log = logging.getLogger("result_frames")
"""
skip current vid frame index, should calculate from ms
"""


def determine_result_frames(source_frames_dir, result_frames_dir, music_timestamps, timestamps_per_video,
                            highlight_timestamp_after_index,
                            video_list, target_fps):
    gf.print_log(f"Determining frames of result video.")
    fm.realise_dir(result_frames_dir, "frames to create the result video from")
    frame_ms = 1000 / target_fps  # 30 fps means 33.33 ms between each frame
    output_ms = 0  # the exact moment where we are with writing frames in our result video
    add_frames_to_video_list(video_list=video_list, frames_dir=source_frames_dir, target_fps=target_fps)

    current_vid_index = 0  # the index of the source video being used
    current_vid = video_list[current_vid_index]
    # the duration of the output video, for process bar, for now the last music timestamp,
    # the music marker puts one timestamp at the exact end of the video
    result_vid_length = music_timestamps[-1]
    frame_count = 0  # how many frames are written to result
    # the index of the first timestamp being used for the current timestamp range
    # start value negative so timestamp range rotation in the loop triggers on first run
    timestamp_index = -timestamps_per_video
    timestamp_range_end_ms = -1  # how many ms in result video the timestamp range ends

    output_video_width, output_video_height = Image.open(current_vid.frame_paths[0]).size
    log.info(f"Frame dimensions: {output_video_width}x{output_video_width}")

    while 1:
        # we reserve a timestamp range for each video. when out of range, go to next range and video
        if timestamp_range_end_ms <= output_ms:
            log.info("Reached the end of the current timestamp range, switching to next range and video.")

            # switch to next timestamp range
            timestamp_index = timestamp_index + timestamps_per_video
            if timestamp_index >= len(music_timestamps) + 1:
                log.info("No more timestamps available for the rest of the videos.")
                break
            end_timestamp_index = timestamp_index + timestamps_per_video
            if end_timestamp_index >= len(music_timestamps) + 1:  # this will probably always trigger first
                log.info("No more timestamps available to cover this video to the end of the timestamp range.")
                break
            timestamp_range_end_ms = music_timestamps[timestamp_index + timestamps_per_video]
            log.info(f"Timestamp index is now {timestamp_index}. End at {timestamp_range_end_ms}ms.")

            # switch to next video
            current_vid_index += 1
            if current_vid_index >= len(video_list) - 1:
                log.info("No more videos available for the rest of the timestamps.")
                break
            current_vid = video_list[current_vid_index]

            # at which music timestamp the highlight should happen
            highlight_timestamp_index = timestamp_index + highlight_timestamp_after_index
            highlight_timestamp = music_timestamps[highlight_timestamp_index]
            # how many ms that timestamp is after the first timestamp of the current timestamp range
            highlight_timestamp_delta = highlight_timestamp - music_timestamps[timestamp_index]
            # how many ms from the beginning of this video should be trimmed to
            # have the highlight sync up with the above determined timestamp
            current_video_prefix_trim = current_vid.highlight_timestamp - highlight_timestamp_delta
            log.info(f"{highlight_timestamp_index=}, {highlight_timestamp=}, "
                     f"{highlight_timestamp_delta=}, {current_video_prefix_trim=}")
            current_vid.discard_ms = current_video_prefix_trim
            if current_vid.discard_ms < 0:
                log.warning(f"Input video too short: {current_vid}")
                break

            # determine at which times within this video the highlight effect should be triggered (slow motion/reversal)
            current_vid.highlight_effect_start_ms = music_timestamps[highlight_timestamp_index - 0]
            current_vid.highlight_effect_stop_ms = music_timestamps[highlight_timestamp_index + 3]
            log.info(f"Highlight special effect: "
                     f"from {current_vid.highlight_effect_start_ms}ms to {current_vid.highlight_effect_stop_ms}ms.")

            # at which times a video effect occurs (speedup)
            current_vid.effect_start_ms = music_timestamps[highlight_timestamp_index - 5]
            current_vid.effect_stop_ms = music_timestamps[highlight_timestamp_index - 4]
            log.info(f"Video effect from {current_vid.effect_start_ms}ms to {current_vid.effect_stop_ms}ms.")

        # update progress ##############################################################################################
        gf.print_log(f"Processing second {round(output_ms // 1000)} / {result_vid_length // 1000}"
                     f" of result video", end="\r")

        # speed effect  ################################################################################################
        speed_multiplier = 1.0

        if current_vid.highlight_effect_start_ms <= output_ms <= current_vid.highlight_effect_stop_ms:
            log.info("Video effect triggered for highlight.")
            speed_multiplier = 2.0

        # update the current videos read progress ######################################################################
        if current_vid.read_ms < current_vid.discard_ms:
            # discard the first few frames of this video to have highlight sync with beat
            current_vid.read_ms = current_vid.discard_ms
            log.info(f"Fast forwarded current video to {current_vid.read_ms} to match timestamp.")
        current_vid.read_ms += frame_ms * speed_multiplier

        # write frame from current video to result video ###############################################################
        current_vid.frame_index = int(current_vid.read_ms // frame_ms)
        log.info(
            f"Reading frame {current_vid.frame_index} of {len(current_vid.frame_paths)} of video #{current_vid.index}.")
        if len(current_vid.frame_paths) - 1 <= current_vid.frame_index:
            log.info("No more frames to write.")
            break
        from_path = current_vid.frame_paths[current_vid.frame_index]
        to_filename = f"{frame_count}_video{current_vid.index}_frame{current_vid.frame_index}.jpg"
        written_frame_path = os.path.join(result_frames_dir, to_filename)
        shutil.copy2(from_path, written_frame_path)  # copy2 keeps metadata or something
        log.info(f"Copied {from_path} to {written_frame_path}")

        # update result video stats
        output_ms += frame_ms
        frame_count += 1

    amount = len([name for name in os.listdir(result_frames_dir)
                  if os.path.isfile(os.path.join(result_frames_dir, name))])
    gf.print_log(f"Written {frame_count} frames for result video! The folder now has {amount} files.")
