# Compilation maker
Create a compilation from videos on the beat of the music!  
Example and inspiration: [Joji - Gimme Love](https://www.youtube.com/watch?v=jPan651rVMs)  
[Demo](https://youtu.be/sEDeM4lWn8Q)

# Manual
Run the scripts from within this folder. Some scripts expect some sources in the `src` folder, for example `font.ttf`.
Logs are saved to `log/compilation_maker_log_xxxxxxxx_xxxxxx`.

### Step 1 - intalling requirements
This software requires Python, and a few libraries.
Install the latest version of python. Then install the required libraries by running:
```shell
pip3 install -r requirements.txt
```

### Step 2 - mark the song
Mark on which moments of the song the video should cut to the next one.  
Run the music marker program with a song, and the path to the file (which not yet exists) where the markers are saved.
```shell
python3 music_marker.py /path/to/song.mpg /path/to/timestamps.yaml
```
With the application open, press spacebar on your keyboard to start playing and marking the song.
Tap any key (a-z) on the beat of the song!
When the music is done playing, or when you press spacebar again, the timestamps of your keypresses are written to 
`/path/to/timestamps.yaml`.

### Step 3 - gather videos
You probably tapped your keyboard like 100-300 times, so you need a lot of videos.
I mostly used the motion / live photos taken from my smartphone.
A motion photo has an embedded video, which shows what happened right before and after the photo was taken.
Those embedded videos can be extracted, with tools you you can find online.  
I have included a simple script for it in the `util` folder: `extract_motion_videos.py`.
This script uses the `motion_photo_splitter` Python package, 
which is able to extract the videos from Samsung motion photos.
```shell
python3 extract_motion_videos.py /path/to/motion/photos/
```
This script outputs the mp4s in a new folder: `/path/to/motion/photos/extracted_motion_videos`

Another way to gather videos is to split a longer video into multiple smaller ones. 
I have included a script for it in the `util` folder: `video_splitter`.  
Usage:
```shell
python3 video_splitter.py /path/to/video/or/folder/with/videos /path/for/output length_of_each_split amount_of_splits_or_percentage
```
Example usage:
```shell
python3 video_splitter.py ~/this/vid/compilation/test_beta/split_test.mp4 ~/this/vid/compilation/test_beta/splits/ 2000 3
```
This splits the `split_test.mp4` into 3 parts of each 2000ms long, and saves them to the `splits/` folder.


### Step 4 - generate the compilation
Generate the compilation!  
```shell
python3 compilation_maker_v2.py /path/to/videos/ /path/to/music.mp3 /path/to/music_timestamps.mp3 /path/to/result/folder
```
This uses the videos found in `/path/to/videos` to create a compilation on the beat of `/path/to/music.mp3` by using
`path/to/music/timestamps` you created with the `music_marker.py` script. It saves the result to 
`path/to/result/folder`, and also creates temporary folders there, like `extracted_frames/` and `trimmed_videos`.

The rendering process takes like 10 minutes on my above average computer. 
It edits the videos to the required resolution, length and framerate, 
then it splits those edited videos into single frames, and puts those back together on the beat.



# Util
This directory contains scripts which can be used to gather the required clips for your compilation.  
For more information of the scripts, see below or the top comment within the scripts code.

### video_splitter.py
Split a single, longer video into multiple shorter ones.

### extract_motion_videos.py
Extract the videos embedded in your motion photos of your Samsung device.

### timestamp_to_filename.py
Set all the filenames in a directory to a formatted date, so that they order chronologically when sorted by name.
This lets you merge captures from different devices, and keep the correct order when other scripts remove the creation date metadata.

### renamer.py
Rename all files in a folder so they are in the correct order and are consistent. (requires programming)

### video_checker.py
Check if your videos are the way you want them to be, for example if they all are 30fps. (requires programming)

# Implementation
## compilation_maker
Prerequisites:
* video clips
* song.mp3
* file with timestamps on the rhythm of the song: `600, 1200, 1800, 2400, 3000, 3600, 4200, 5800 etc`

Operation:
* determine for each video: to which two adjacent timestamps it corresponds, and roughly slow down or trim video accordingly
* edit the trimmed videos: set fps, resolution, and text (video number for example)
* extract all the frames of all the video clips into a folder: `video1_frame1, video1_frame2 ... video2_frame1, video2_frame2`
* iterate through the extracted frames, writing just enough frames to the result video so it perfectly syncs with the music timestamps
* do this by keeping track how many ms is read from the current video and is written to the result video
* use ffmpeg to create a video of all the written frames and song

## highlight compilation maker
Prerequisites: 
* video clips
* for each video clip a separate file with the timestamp of the highlight of that video clip
* song.mp3
* file with timestamps on the rhythm of the song: `600, 1200, 1800, 2400, 3000, 3600, 4200, 5800 etc`

Operation:
* extract all the frames of all the video clips into a folder: `video1_frame1, video1_frame2 ... video2_frame1, video2_frame2`
* pick a timestamp range, for example 1200 to 4200
* determine on which timestamp within that timestamp range the highlight should be, for example 3000
* determine how much of the current video clip needs to be trimmed in order to sync the highlight with the timestamp
* iterate through the extracted frames, skipping frames which needs to be trimmed and writing the other frames to the result video
* (keep track how many ms is read from the current video and written to the result video)
* if the highlight matches the timestamp, freeze the frames until the next timestamp (or add effects)
* use ffmpeg to create a video of all the written frames and song


# TODO
* Add demo.
* Option to also use photos (maybe animate those)
* cut up videos if not enough videos
* only force aspect ratio, not resolution on higher res files
* add music progress to music marker
* flicker frames surrounding highlight
* at end show all the highlights in quick succession
* sharpen highlights or increase brightness a bit
* clean up osrs code
* create walking clips compilation maker thing: walk on the beat
* Miss zoom + slowmo in middle of screen (dus op character) voor de kill
* En flashes + slowmo als je rent
* Met shake drop on arrival & kill
* https://youtu.be/U7X6pdVM9cA?t=82


# Could have
* identify all type of rithms within song: the bass, the clap, etc (maybe search for peaks with rithm, show on timeline)
* slow down / speed ups / zoom in instead of video cuts
* slow transitions for more quiet parts
* flickering on fast parts (2-4-8-16 16 idea)
* tessa violet - crush type loops on beat, single video input, or multiple and switch on second marking by music_marker
* android app: list of videos, each of which can be: played(within list), sorted, removed, splitted, show time between timestamps
* accept motion photos
* show general photo location in video
* length matching: longer pauses fit longer vids, etc
